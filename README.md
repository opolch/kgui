# kgui - generic katcp gui

## How to invoke

```python
python3.7 kGui.py
```

## Requirements

### UNIX

* `3.7<=python<=3.10`
* qtawesome
* pyqt5
* pyqtgraph
* katcp

`see requirements.txt`

### Windows

Some combinations of Python and katcp/tornado give unexpected results. One should stick to the setup mentioned in `requirements_windows.txt` which is proven to be working.

## Git Submodule fetch with Cloning

```
git clone --recurse-submodules <git_repo_url>
```

## Conda Integration

### Installation and ENVs

* Install some conda (e.g. https://docs.anaconda.com/miniconda/)
* Create environment `conda create -n kgui_3.7 python=3.7`
* Activate env `conda activate kgui_3.7`
* Install required packages
  * Install requirements `pip install -r requirements.txt` (UNIX)
  * Install requirements `pip install -r requirements_windows.txt` (Windows)

### Starting kGui

#### UNIX

The startup can be realized by combining a desktop-file and a shell script. The shell script has the purpose to load the conda environt and to execute the kgui application, while the desktop-file implements an option to execute the shell script from a desktop icon.

> Example working with XFCE4 desktop environment (others similar)

* desktop-file
```shell
[Desktop Entry]
Version=1.0
Type=Application
Terminal=true
Name=kgui
Comment=kgui
StartupNotify=false
Icon=
Exec=/path/to/shell_script.sh
```

* shell script
```shell
#!/bin/bash
#       /\ your shell

# Set path to kgui directory
CONDA_SH="/path/to/etc/profile.d/conda.sh" # Or path to where your conda is"
KGUI_DIR="/path/to/kgui"


# Activate the conda env
source "$CONDA_SH"
conda activate kgui_3.7

# Start the application with specific configuration
python3 "$KGUI_DIR"/kGui.py -c rsc -s "rxs:localhost:53121:rxs"
```

#### Windows

Following bash script will start kGui through an existing conda environment:
```
@echo off
call C:\ProgramData\Miniconda\Scripts\activate.bat
python3 "D:\folder\kGui.py"
call C:\ProgramData\Miniconda\Scripts\deactivate.bat
```
Create a desktop shortcut from that file to be able to execute the application via a link.

## Known Issues

### pyqtgraph and numpy -- Cannot cast ufunc multiply output from dtype('float64') to ... with casting rule 'same_kind'

With old versions of packer we saw the following error occurring:

``` python
.../pyqtgraph/functions.py, line 1222
Cannot cast ufunc multiply output from dtype('float64') to dtype('int16') with casting rule 'same_kind'
```
This was proven to be related to some inconsistency in the snapshot histogram data. To work around it a change to the corresponding file must be made as shown below:

``` python
# .../pyqtgraph/functions.py, around lines 1222-1223
out = np.core.umath.minimum(arr, vmax, out=out, casting='unsafe')
return np.core.umath.maximum(out, vmin, out=out, casting='unsafe')
```

## Contribute

Developers please check out the most current revision from the master branch and create pull requests.