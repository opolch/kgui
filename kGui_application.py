import sys

from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt, QMargins

from epl.epl_lists import epl_lists_slice
from epl.epl_qt import epl_qt_tables, epl_qt_forms, epl_qt_tabs, epl_qt_threads
from epl.epl_katcp.epl_katcp_command import CommandState, CommandType
from epl.epl_katcp.epl_katcp_sensor import KatcpSensor
from epl.epl_katcp.epl_katcp_client import KatcpClient
from epl.epl_qt.epl_qt_buttons import PushButton
from epl.epl_qt.epl_qt_grids import Grid
from epl.epl_qt.epl_qt_labels import Label
from epl.epl_qt.epl_qt_plot import Plotter
from epl.epl_qt.epl_qt_dialogs import MessageBox
from epl.epl_string import epl_string_split
import pyqtgraph.exporters
import pyqtgraph

from datetime import datetime

import numpy as np

# Make numpy raise all errors
np.seterr(all='raise')


class MainApplication(QApplication):
    class WindowPacketizer(epl_qt_forms.Window):
        """
        Class derived from epl.epl_qt_forms.Window to provide packetizer window functionality.
        """

        class WindowTsys(epl_qt_forms.Window):
            """
            Class derived from epl.epl_qt_forms.Window to implement the Tsys functionality
            """

            def __init__(self, katcp_client):
                """
                Init the window.
                :param katcp_client: The packetizer katcp client.
                """

                # Variables
                self._katcp_client = katcp_client
                self._integ_time = 0.0
                self._t_hot = 0.0
                self._t_cold = 0.0
                self._spectra_data = None

                # Grid widget for the window layout
                self._grid_widget = Grid()

                # Init base class
                super().__init__("{}:{}".format('Tsys', self._katcp_client.device_name), 40, 60, 800, 600,
                                 box_layout=self.BoxLayout.VERTICAL)

                # Setup window elements
                self._ledit_integ_time = QLineEdit()
                self._ledit_t_hot = QLineEdit()
                self._ledit_t_cold = QLineEdit()
                self._ledit_channel = QLineEdit()
                btn_set_integ_time = PushButton(object_name='integ',
                                                button_text='Set IntegTime',
                                                icon_standard_qta='mdi.arrow-right-circle-outline',
                                                icon_active_qta='fa.spinner',
                                                icon_success_qta='fa5.check-circle',
                                                clicked_slot=self.btn_integ_time_click,
                                                max_timeout_reset=0.5,
                                                timeout_error_functionality=False)
                btn_take_hot = PushButton(object_name='take_hot',
                                          button_text='Take HOT',
                                          icon_standard_qta='mdi.arrow-right-circle-outline',
                                          icon_active_qta='fa.spinner',
                                          icon_success_qta='fa5.check-circle',
                                          clicked_slot=self.btn_take_hot_click,
                                          max_timeout_reset=0.5,
                                          timeout_error_functionality=False)
                btn_take_cold = PushButton(object_name='take_cold',
                                           button_text='Take COLD',
                                           icon_standard_qta='mdi.arrow-right-circle-outline',
                                           icon_active_qta='fa.spinner',
                                           icon_success_qta='fa5.check-circle',
                                           clicked_slot=self.btn_take_cold_click,
                                           max_timeout_reset=0.5,
                                           timeout_error_functionality=False)
                btn_calculate = PushButton(object_name='calculate',
                                           button_text='Calculate',
                                           icon_standard_qta='mdi.arrow-right-circle-outline',
                                           icon_active_qta='fa.spinner',
                                           icon_success_qta='fa5.check-circle',
                                           clicked_slot=self.btn_calculate_click,
                                           max_timeout_reset=0.1,
                                           timeout_error_functionality=False)

                # Layout for integration time row
                integ_layout = QHBoxLayout()
                integ_layout.addWidget(QLabel('IntegTime/s'))
                integ_layout.addWidget(self._ledit_integ_time)
                integ_layout.addWidget(btn_set_integ_time)

                # Layout for calculation settings row
                set_layout = QHBoxLayout()
                set_layout.addWidget(QLabel('Thot/K'))
                set_layout.addWidget(self._ledit_t_hot)
                set_layout.addWidget(QLabel('Tcold/K'))
                set_layout.addWidget(self._ledit_t_cold)
                set_layout.addWidget(QLabel('Ch[\'\'|\'adc0\'|\'adc1\']'))
                set_layout.addWidget(self._ledit_channel)

                # Layout for button row
                btn_layout = QHBoxLayout()
                btn_layout.addWidget(btn_take_hot)
                btn_layout.addWidget(btn_take_cold)
                btn_layout.addWidget(btn_calculate)

                # Setup spectra graph
                self.graph_spectra = Plotter(show_legend=True,
                                             plots=['adc0', 'adc1'],
                                             label_x='freq',
                                             unit_x='Hz',
                                             label_y='Y',
                                             unit_y='#',
                                             log_y=False,
                                             graph_title='Tsys',
                                             show_grid=True,
                                             background=Plotter.GraphColors.AUTO,
                                             symbols=[Plotter.GraphSymbols.NONE, Plotter.GraphSymbols.NONE],
                                             pen_colors=[Plotter.GraphColors.CYAN, Plotter.GraphColors.GREEN]
                                             )

                # Built up windows
                widget = QWidget()
                win_layout = QVBoxLayout()
                win_layout.addLayout(integ_layout)
                win_layout.addLayout(set_layout)
                win_layout.addLayout(btn_layout)
                win_layout.addWidget(self.graph_spectra)
                widget.setLayout(win_layout)
                self.add_widget(widget)

                # Connect the spectra ready signal
                self._katcp_client.signal_handler.signal_data_ready.connect(self.spectra_data_available)

                # Data storage for the
                self.data_hot = None
                self.data_cold = None

            def spectra_data_available(self):
                self._spectra_data = self._katcp_client.snapshot_spec_data[1].get(False)

            def btn_integ_time_click(self):
                try:
                    # Get the sending button
                    sending_button = self.sender()

                    # Disconnect button signal and set active icon
                    sending_button.pending_set()

                    self._integ_time = float(self._ledit_integ_time.text())

                    self._katcp_client.snapshot_spec_enable_request(self._integ_time)
                except Exception as e:
                    MessageBox(QMessageBox.Warning,
                               'Invalid values',
                               'At least one value was not within the limits.',
                               QMessageBox.Ok)

            def btn_take_hot_click(self):
                # Get the sending button
                sending_button = self.sender()

                # Disconnect button signal and set active icon
                sending_button.pending_set()

                # Read out HOT data
                self.data_hot = [self._spectra_data.header_data,
                                 self._spectra_data.signal_data['adc0'].signal_data.copy(),
                                 self._spectra_data.signal_data['adc1'].signal_data.copy()]

            def btn_take_cold_click(self):
                # Get the sending button
                sending_button = self.sender()

                # Disconnect button signal and set active icon
                sending_button.pending_set()

                # Read out COLD data
                self.data_cold = [self._spectra_data.header_data,
                                  self._spectra_data.signal_data['adc0'].signal_data.copy(),
                                  self._spectra_data.signal_data['adc1'].signal_data.copy()]

            def btn_calculate_click(self):

                try:
                    # Get the sending button
                    sending_button = self.sender()

                    # Disconnect button signal and set active icon
                    sending_button.pending_set()

                    ###
                    # Calculation: TRX = (Thot-Y*Tcold) / (Y-1)
                    ###

                    self._t_hot = int(self._ledit_t_hot.text())
                    self._t_cold = int(self._ledit_t_cold.text())

                    # adc0
                    data_y = np.divide(np.power(10, np.divide(self.data_hot[1], 10)),
                                       np.power(10, np.divide(self.data_cold[1], 10)))
                    data_adc0 = np.multiply(data_y, self._t_cold)
                    data_adc0 = np.subtract(self._t_hot, data_adc0)
                    data_y = np.subtract(data_y, 1.0)

                    # Replace 0s to prevent 0-division error
                    for i in range(0, len(data_y)):
                        if i > 0:
                            if data_y[i] <= 0.0:
                                data_y[i] = data_y[i - 1]
                        else:
                            data_y[i] = 0.1

                    data_adc0 = np.divide(data_adc0, data_y)

                    # adc1
                    data_y = np.divide(np.power(10, np.divide(self.data_hot[2], 10)),
                                       np.power(10, np.divide(self.data_cold[2], 10)))
                    data_adc1 = np.multiply(data_y, self._t_cold)
                    data_adc1 = np.subtract(self._t_hot, data_adc1)
                    data_y = np.subtract(data_y, 1.0)

                    # Replace 0s to prevent 0-division error
                    for i in range(0, len(data_y)):
                        if i > 0:
                            if data_y[i] <= 0.0:
                                data_y[i] = data_y[i - 1]
                        else:
                            data_y[i] = 0.1

                    data_adc1 = np.divide(data_adc1, data_y)

                    # Remove first element (dc error)
                    spectra_x_data = list()
                    for i in range(1, self.data_cold[0].num_channels):
                        spectra_x_data.append(
                            self.data_cold[0].band_width_adc * i / (self.data_cold[0].num_channels - 1))

                    # Update the plots
                    if self._ledit_channel.text() == '' or self._ledit_channel.text() == 'adc0':
                        self.graph_spectra.plot(data_x=spectra_x_data,
                                                data_y=data_adc0,
                                                plot_name='adc0')
                    else:
                        self.graph_spectra.plot(data_x=[0],
                                                data_y=[0],
                                                plot_name='adc0')
                    if self._ledit_channel.text() == '' or self._ledit_channel.text() == 'adc1':
                        self.graph_spectra.plot(data_x=spectra_x_data,
                                                data_y=data_adc1,
                                                plot_name='adc1')
                    else:
                        self.graph_spectra.plot(data_x=[0],
                                                data_y=[0],
                                                plot_name='adc1')

                except Exception as e:
                    MessageBox(QMessageBox.Critical,
                               'Calculation Error',
                               'Error during calculation. \n\nErr: {}'.format(e),
                               QMessageBox.Ok)

        def __init__(self, katcp_client, logger):

            super().__init__(katcp_client.device_name, 40, 60, 800, 600, box_layout=self.BoxLayout.VERTICAL)

            # Logger
            self._logger = logger

            # Katcp client
            self.katcp_client = katcp_client

            # Connect update signal
            self.katcp_client.signal_handler.signal_data_ready.connect(self.update_graph_data)

            # Spectra x-data list. No need to recalculate every time data is updated
            self._spectra_x_data = list()

            # Labels
            self.lb_last_update = None

            # Window for Tsys calculation
            self._window_tsys = None

            # Init main window
            self._init_main_window()

        def _init_main_window(self):
            """
            Creates the main window with all the widgets and elements.
            :return:
            """
            # Setup spectra graph
            self.graph_spectra = Plotter(show_legend=True,
                                         plots=['adc0', 'adc1'],
                                         label_x='freq',
                                         unit_x='Hz',
                                         label_y='Y',
                                         unit_y='dBm',
                                         log_y=False,
                                         graph_title='Spectra-Snapshot',
                                         show_grid=True,
                                         background=Plotter.GraphColors.AUTO,
                                         symbols=[Plotter.GraphSymbols.NONE, Plotter.GraphSymbols.NONE],
                                         pen_colors=[Plotter.GraphColors.CYAN, Plotter.GraphColors.GREEN]
                                         )

            # Setup level graphs
            self.graph_level0 = Plotter(show_legend=False,
                                        plots=['level0'],
                                        label_y='Y',
                                        graph_title='adc0',
                                        show_grid=True,
                                        symbols=[Plotter.GraphSymbols.NONE],
                                        pen_colors=[Plotter.GraphColors.CYAN],
                                        background=Plotter.GraphColors.AUTO)
            self.graph_level1 = Plotter(show_legend=False,
                                        plots=['level1'],
                                        label_y='Y',
                                        graph_title='adc1',
                                        show_grid=True,
                                        symbols=[Plotter.GraphSymbols.NONE],
                                        pen_colors=[Plotter.GraphColors.GREEN],
                                        background=Plotter.GraphColors.AUTO)

            # Level graphs layout
            layout_levels = QHBoxLayout()
            layout_levels.addWidget(self.graph_level0)
            layout_levels.addWidget(self.graph_level1)

            # Add all the widgets
            self.add_widget(self.graph_spectra)
            self.add_layout(layout_levels)

            # Nav bar
            self.add_layout(self._init_navigation_bar())

            # Connect window close signal to close all sub windows as well
            self.close_signal.connect(self.hide_window)

        def _init_navigation_bar(self):
            """
            Inits the actions navigation bar.
            :return: The actions bar layout.
            """
            # Button to show the graph's window
            btn_tsys = PushButton(object_name='tsys',
                                  button_text='Tsys',
                                  clicked_slot=self._btn_tsys_click)

            # Button for packetizer functionality
            btn_waterfall = PushButton(object_name='waterfall',
                                       button_text='Waterfall',
                                       clicked_slot=self._btn_waterfall_click)

            self.lb_last_update = Label('Last Updated', width=100, alignment=(Qt.AlignVCenter | Qt.AlignRight))

            # Add everything to a HBoxLayout
            settings_layout = QHBoxLayout()
            settings_layout.setContentsMargins(10, 0, 10, 5)
            settings_layout.addWidget(btn_tsys)
            settings_layout.addWidget(btn_waterfall)
            settings_layout.addWidget(self.lb_last_update)

            return settings_layout

        def update_graph_data(self):
            """
            Updates graphs data of all graphs.
            :return:
            """
            try:
                # Get last spectra data
                spec_data = self.katcp_client.snapshot_spec_data[0].get()

                # Init the x-data on first run
                if len(self._spectra_x_data) == 0:
                    # Remove first element (dc error)
                    for i in range(1, spec_data.header_data.num_channels):
                        self._spectra_x_data.append(
                            spec_data.header_data.band_width_adc * i / (spec_data.header_data.num_channels - 1))

                # Update the plots
                self.graph_spectra.plot(data_x=self._spectra_x_data,
                                        data_y=spec_data.signal_data['adc0'].signal_data,
                                        plot_name='adc0')
                self.graph_spectra.plot(data_x=self._spectra_x_data,
                                        data_y=spec_data.signal_data['adc1'].signal_data,
                                        plot_name='adc1')
                self.graph_level0.plot(data_y=spec_data.level_data['level0'].level_data,
                                       plot_name='level0')
                self.graph_level1.plot(data_y=spec_data.level_data['level1'].level_data,
                                       plot_name='level1')

                self.lb_last_update.set_text('last {}'.format(datetime.now().strftime('%X')))

            except Exception as e:
                self._logger.write_exception('Error updating spectra data.')

        def _btn_tsys_click(self):
            """
            Setup window for the tsys determining.
            :return:
            """
            if self._window_tsys is None:
                self._window_tsys = self.WindowTsys(self.katcp_client)
            self._window_tsys.show_window()

        def _btn_waterfall_click(self):
            """
            Placeholder
            :return:
            """
            pass

        def hide_window(self):
            """
            Override the hide_window callback to also hide sub_windows.
            :return:
            """
            if self._window_tsys is not None:
                self._window_tsys.hide_window()

            # Disable spectra snapshots
            self.katcp_client.snapshot_spec_enable_request(enable=False)

            super().hide_window()

    def __init__(self, application_name):
        """
        A basic init (simply calling base __init__) and defining the instance members.
        Window init over init functions.
        :param application_name: Application title, shown in title bar.
        """

        # Init parent class
        super().__init__(sys.argv)

        self._katcp_clients = None
        self._logger = None

        # Set up the application window
        self.quit_signal = self.aboutToQuit
        self._window_main = epl_qt_forms.Window(application_name, 40, 60, 800, 600,
                                                box_layout=epl_qt_forms.Window.BoxLayout.VERTICAL)
        self._window_main.close_signal.connect(self._hide_sub_windows)
        self._window_main.show_window()

        # Setup graph view
        self._window_graph = None
        self._graph_grid = None

        # List for packetizer views
        self._windows_packetizers = None

        # Create tab view, add a tab for each clipboard
        self._tab_views = None

        # Storing list -> used for updating sensor values automated
        self._tab_view_tables = None
        self._tab_view_sensor_list = None
        self._tab_view_command_list = None

        # Threads
        self.update_thread_table = None
        self.update_thread_graphs = None

        # Labels
        self._lb_last_update = None

    def init_main_window(self, katcp_clients, clipboards, logger):
        """
        Setup the Application. Init windows and forms.
        :param katcp_clients: dict holding KatcpClient clients to be queried.
        :param clipboards: List with clipboards to be shown as tabs.
        :param logger: epl_logging_log Log.
        :return:
        """

        # Set variables
        self._katcp_clients = katcp_clients
        self._logger = logger

        # Setup graph view
        self._window_graph = epl_qt_forms.Window('Graph-View', 40, 60, 800, 600)
        self._graph_grid = Grid()
        self._window_graph.setLayout(self._graph_grid)

        # List for packetizer views
        self._windows_packetizers = dict()

        # Create tab view, add a tab for each clipboard
        self._tab_views = epl_qt_tabs.TabView(x for x in clipboards.keys())

        # Storing list -> used for updating sensor values automated
        self._tab_view_tables = [[] for _ in clipboards.keys()]
        self._tab_view_sensor_list = [[] for _ in clipboards.keys()]
        self._tab_view_command_list = [[] for _ in clipboards.keys()]

        # Setup clipboards
        count_clipboards = 0
        for clipboard_name, clipboard_items in clipboards.items():

            # The tab layout
            tab_layout = QHBoxLayout()

            for table_name, table_items in clipboard_items.items():

                # List with clients being not available
                clients_not_available = list()

                # Create table list to setup table widget
                table_list = list()

                # Iterate through the table layouts
                for sub_table_name, sub_table_items in table_items.items():

                    # Sub table list with own header
                    sub_table_list = list()

                    # Add emtpy entries for sub table headers
                    # Necessary for update thread
                    self._tab_view_sensor_list[count_clipboards].append(None)
                    self._tab_view_command_list[count_clipboards].append(None)

                    for row in sub_table_items:
                        # Read out values
                        client_name, sensor_name, sensor_name_friendly, command_name, command_name_friendly = row[0:5]
                        # Client name is just the index of the clients in the katcp_clients dict
                        client_name = list(self._katcp_clients.keys())[int(client_name)]

                        # Initialize the string with not connected
                        sensor_value_string = 'client not connected'
                        sensor = None
                        command = None

                        try:
                            # Check if client is connected
                            if self._katcp_clients[client_name].is_connected() is True:
                                # Get sensor from client's sensor map
                                if sensor_name != '':
                                    for sensor_map_name, sensor_map_sensor in \
                                            self._katcp_clients[client_name].SENSOR_MAP.items():
                                        # Do kind of wildcard look-up
                                        if sensor_name in sensor_map_name:
                                            sensor = sensor_map_sensor
                                            sensor_name = sensor_map_name
                                            sensor_value_string = 'not updated yet'
                                            break
                                if sensor is None:
                                    if sensor_name == '':
                                        # Command only parameter
                                        sensor_value_string = '[command only]'
                                    else:
                                        # Mark client/sensor as not available
                                        sensor_value_string = 'sensor not available'

                                if command_name != '':
                                    for command_map_name, command_map_command in \
                                            self._katcp_clients[client_name].COMMAND_MAP.items():
                                        # Do kind of wildcard look-up
                                        if command_name in command_map_name:
                                            command = command_map_command
                                            break

                        except Exception as e:
                            # Mark client/sensor as not available
                            sensor_value_string = 'client not available'
                            clients_not_available.append(client_name)
                        finally:
                            # Create buttons
                            if command is not None:  # Command button only if command is available
                                command_button = PushButton(object_name=client_name + ':' + command.command_name,
                                                            icon_standard_qta='mdi.arrow-right-circle-outline',
                                                            icon_active_qta='fa.spinner',
                                                            icon_success_qta='fa5.check-circle',
                                                            icon_error_qta='mdi.alert-circle-outline',
                                                            icon_size_qta=[20, 20],
                                                            clicked_slot=self._btn_command_click,
                                                            max_timeout_reset=10)
                            else:
                                command_button = PushButton(object_name=sensor_name,
                                                            icon_standard_qta='fa5.window-close',
                                                            icon_size_qta=[20, 20], enabled=False)

                            set_button = PushButton(object_name=client_name + ':' + sensor_name,
                                                    icon_standard_qta='mdi.wrench-outline',
                                                    icon_size_qta=[20, 20], clicked_slot=self._btn_options_click)

                            # Display command name?
                            if command_name_friendly != '':
                                if sensor_name_friendly != "":
                                    sensor_name_friendly += '\n(' + command_name_friendly + ')'
                                else:
                                    sensor_name_friendly = command_name_friendly

                            # Generate table entry
                            sub_table_list.append(
                                [client_name, sensor_name_friendly, sensor_value_string, command_button, set_button])

                            # Save the related sensors/commands, the list is used for updating table values later
                            self._tab_view_sensor_list[count_clipboards].append(sensor)
                            self._tab_view_command_list[count_clipboards].append(command)

                    # Add sub table to table list
                    table_list.append({sub_table_name: sub_table_list})

                # Create the table widget
                table = epl_qt_tables.Table(table_list,
                                            show_horizontal_header=True,
                                            show_vertical_header=True,
                                            horizontal_header_items=['function', 'value(unit)', 'set', 'opts'],
                                            resize_modes_horizontal=[QHeaderView.Stretch,
                                                                     QHeaderView.Stretch,
                                                                     QHeaderView.ResizeToContents,
                                                                     QHeaderView.ResizeToContents,
                                                                     QHeaderView.ResizeToContents,
                                                                     QHeaderView.ResizeToContents,
                                                                     QHeaderView.ResizeToContents],
                                            resize_modes_vertical=[QHeaderView.Stretch])

                # Highlight clients which are not available
                self._update_unavailable_devices(clients_not_available, table)

                # Save the table widget, the dict is used for updating sensor values later
                self._tab_view_tables[count_clipboards].append(table)

                # Add table widget to tab-layout
                tab_layout.addWidget(table.tableWidget)

            # Set tab layout
            self._tab_views.tab_layout_set(clipboard_name, tab_layout)

            # Count up clipboard counter
            count_clipboards += 1

        # Create settings navigation bar
        nav_bar_layout = self._init_navigation_bar()

        self._window_main.add_widget(self._tab_views)

        # Add navigation bar to main window
        self._window_main.add_layout(nav_bar_layout)

        # Start the table update thread
        self.update_thread_table = epl_qt_threads.UpdateThread(0.5)
        self.update_thread_table.update_signal.connect(self._update_table_values)
        self.update_thread_table.start()

        # Start the graphs update thread
        self.update_thread_graphs = epl_qt_threads.UpdateThread(0.5)
        self.update_thread_graphs.update_signal.connect(self._update_graph_window)
        self.update_thread_graphs.start()

    def server_config_select(self, config_options):

        # TODO: Use dialog menu module
        dialog_wid = QWidget()
        set_value, ok_pressed = QInputDialog.getItem(dialog_wid,
                                                     'Config',
                                                     'Choose Configuration:',
                                                     ['{} :-: {}'.format(config_name, config_settings['description'])
                                                      for config_name, config_settings in config_options.items()],
                                                     0,
                                                     False)
        if ok_pressed is True:
            return epl_string_split.split_index(set_value, separator=' :-: ')
        else:
            return None

    def server_select(self, servers):
        """
        Displays a dialog window to select servers during application start.
        :param servers: dict with servers from config file.
        :return: list with selected servers.
        """
        # Return list
        selected_servers = list()

        # Iterate over servers to let user select
        server_counter = 0
        for server in servers:
            server_counter += 1

            # TODO: Use dialog menu module
            dialog_wid = QWidget()
            set_value, ok_pressed = QInputDialog.getItem(dialog_wid,
                                                         'Config',
                                                         'Choose Device {}/{}:'.format(server_counter, len(servers)),
                                                         ['{}'.format(key) for key in server[0]],
                                                         0,
                                                         False)
            if ok_pressed is True:
                selected_servers.append(server[0][set_value])
            else:
                selected_servers = None
                return selected_servers

        return selected_servers

    def special_function_add(self, katcp_client):
        """
        Adds a special function to the GUI view.
        :param katcp_client: KatcpClient with special function.
        :return:
        """

        # Init the spectra snapshot view
        if katcp_client.device_type.value == KatcpClient.DeviceType.PACKETIZER.value:
            self._packetizer_window_init(katcp_client)

    def _packetizer_window_init(self, katcp_client):
        """
        Inits an object of type WindowPacketizer and adds it to packetizers windows list.
        :param katcp_client: KatcpClient of packetizer.
        :return:
        """
        pack_window = self.WindowPacketizer(katcp_client, self._logger)
        self._windows_packetizers[katcp_client.device_name] = pack_window

    def _init_navigation_bar(self):
        """
        Inits the actions navigation bar.
        :return: The actions bar layout.
        """
        # Button to show the graph's window
        btn_show_graphs = PushButton(object_name='show_graphs',
                                     button_text='Show Graphs',
                                     clicked_slot=self._show_graphs_window)

        # Button for packetizer functionality
        btn_packetizer = PushButton(object_name='packetizers',
                                    button_text='Packetizer(s)',
                                    clicked_slot=self._btn_packetiters_click)

        self._lb_last_update = Label('Last Updated', width=100, alignment=(Qt.AlignVCenter | Qt.AlignRight))

        # Add everything to a HBoxLayout
        settings_layout = QHBoxLayout()
        settings_layout.setContentsMargins(10, 5, 10, 5)
        settings_layout.addWidget(btn_show_graphs)
        settings_layout.addWidget(btn_packetizer)
        settings_layout.addWidget(self._lb_last_update)

        return settings_layout

    def _update_unavailable_devices(self, devices_not_available, table):
        """
        Highlights devices which are unavailable.
        :param devices_not_available:
        :param table:
        :return:
        """
        for client_name in devices_not_available:
            for i in range(0, table.tableWidget.rowCount()):
                header_client_name = table.header_value_get(i, False)
                if header_client_name == client_name:
                    # TODO: All colors should come from config file
                    table.update_cell_color_row(i, 1, 'client not available', QColor(247, 241, 241))

    def _update_graph_window(self):
        """
        Updates the graphs on the graphs view.
        :return:
        """
        # Update graphs only if window is visible
        if self._window_graph.isVisible() is True:
            # Update graphs
            for widget_name, graph_widget in self._graph_grid.widgets.items():
                for plot in graph_widget.plot_refs.keys():
                    # Extract client and sensor name from button name
                    client_name, sensor_name = plot.split(':')
                    update_sensor = self._katcp_clients[client_name].SENSOR_MAP[sensor_name]

                    graph_widget.plot(data_x=epl_lists_slice.single_list_from_list_of_lists(
                        update_sensor.value_list, 0),
                        data_y=epl_lists_slice.single_list_from_list_of_lists(
                            update_sensor.value_list, 2),
                        plot_name=plot)

    def _update_table_values(self):
        """
        Iterates over the active table and updates sensors and command states.
        :return:
        """
        try:
            update_tables = self._tab_view_tables[self._tab_views.currentIndex()]
            update_sensors = self._tab_view_sensor_list[self._tab_views.currentIndex()]
            update_commands = self._tab_view_command_list[self._tab_views.currentIndex()]

            any_update = False

            # Iterate over all tables on the tab
            j = 0
            for table in update_tables:

                # Iterate over rows
                for i in range(0, table.tableWidget.rowCount()):

                    # Update sensors
                    if update_sensors[j] is not None:

                        # Check for new value
                        if update_sensors[j].was_updated() is True:

                            # Set update indicator
                            any_update = True

                            # TODO: Colors must come from config file
                            # Color based on state
                            row_color = QColor(255, 255, 255)
                            sensor_value = update_sensors[j].read()

                            # Check if value was already updated
                            if sensor_value is not None:
                                if sensor_value[1] == KatcpSensor.SensorStatus.nominal.value:
                                    pass
                                elif sensor_value[1] == KatcpSensor.SensorStatus.warn.value:
                                    row_color = QColor(255, 255, 153)
                                else:
                                    row_color = QColor(255, 102, 102)

                                # Limit digits for float sensors
                                try:
                                    sensor_value_format = '{:.4g}'.format(sensor_value[2])
                                except:
                                    sensor_value_format = '{}'.format(sensor_value[2])

                                # Update the entry
                                if update_sensors[j].units.lower() != 'none' and update_sensors[j]. \
                                        units.lower() != 'discrete':
                                    table.update_cell_color_row(i, 1,
                                                                '{} ({})'.format(sensor_value_format,
                                                                                 update_sensors[j].units),
                                                                color=row_color)
                                else:
                                    table.update_cell_color_row(i, 1,
                                                                '{}'.format(sensor_value_format),
                                                                color=row_color)
                            else:
                                self._logger.write_debug('Sensor {} was not updated yet'.format(update_sensors[j].name))

                    # Update commands
                    if update_commands[j] is not None:
                        if update_commands[j].was_updated is True:
                            update_commands[j].was_updated = False

                            # Get button
                            command_button = table.cell_widget_get(i, 2)

                            # Reset the pending request (activate clicked signal again)
                            command_button.pending_reset()

                            # Set based on state
                            if update_commands[j].command_state != CommandState.OK:
                                command_button.error_set_period()
                            else:
                                command_button.success_set_period()
                    j += 1

            # Check for disconnected clients
            for katcp_client_name, katcp_client in self._katcp_clients.items():
                if katcp_client.is_connected() is False:
                    for table in update_tables:
                        for i in range(0, table.tableWidget.rowCount()):
                            header_client_name = table.header_value_get(i, False)
                            if header_client_name == katcp_client_name:
                                table.update_cell_color_row(i, 1, 'client not connected', QColor(247, 241, 241))

            # Update the last updated time
            if any_update is True:
                self._lb_last_update.set_text('last {}'.format(datetime.now().strftime('%X')))

        except Exception as e:
            self._logger.write_exception('Error in update thread.')

    def _sensor_history_clear(self, client_name, sensor_name):
        """
        Clears a sensor's history data.
        Keeps one element to prevent exceptions.
        :param client_name: katcp client name.
        :param sensor_name: sensor name.
        :return:
        """
        sensor = self._katcp_clients[client_name].sensor_map_get()[sensor_name]
        sensor.value_list_clear(keep_one_element=True)

    def _btn_packetiters_click(self):
        """
        Slot for packetizer button.
        Used to show packetizer windows.
        :return:
        """

        # Check if at least one packetizer available in config
        if len(self._windows_packetizers) == 0:
            MessageBox(QMessageBox.Warning,
                       'No Packetizer available',
                       'There is no packetizer available in current configuration.',
                       QMessageBox.Ok)
            return
        elif len(self._windows_packetizers) == 1:
            # Show packetizer window
            list(self._windows_packetizers.values())[0].show_window()

            # Enable spectra snapshots
            list(self._windows_packetizers.values())[0].katcp_client.snapshot_spec_enable_request(enable=True)

            return

        # TODO: Use dialog menu module
        # Create the widget and choose what kind of dialog to show
        dialog_wid = QWidget()
        set_value, ok_pressed = QInputDialog.getItem(dialog_wid,
                                                     'Packetizer',
                                                     'Choose Packetizer:',
                                                     [client.device_name for _, client in self._katcp_clients.items() if
                                                      client.device_type.value == KatcpClient.DeviceType.PACKETIZER.value],
                                                     0,
                                                     False)
        if ok_pressed is True:
            # Show packetizer window
            self._windows_packetizers[set_value].show_window()

            # Enable spectra snapshots
            self._windows_packetizers[set_value].katcp_client.snapshot_spec_enable_request(enable=True)

    def _btn_command_click(self):
        """
        General slot for command button signal.
        :return:
        """
        try:
            # Get the sending button
            sending_button = self.sender()

            # Extract client and command name from button name
            client_name, command_name = sending_button.objectName().split(':')
            kat_client = self._katcp_clients[client_name]
            katcp_command = kat_client.COMMAND_MAP[command_name]

            # Create the widget and choose what kind of dialog to show
            dialog_wid = QWidget()

            # Based on command type, distinguish what kind of dialog to show
            if katcp_command.command_type is CommandType.INT:
                set_value, ok_pressed = QInputDialog.getInt(dialog_wid, sending_button.objectName(), '')
            elif katcp_command.command_type is CommandType.FLOAT:
                set_value, ok_pressed = QInputDialog.getDouble(dialog_wid, sending_button.objectName(), '', decimals=4)
            elif katcp_command.command_type is CommandType.DISCRETE:
                set_value, ok_pressed = QInputDialog.getItem(dialog_wid, sending_button.objectName(), '',
                                                             katcp_command.values, 0, False)
            else:
                set_value, ok_pressed = QInputDialog.getText(dialog_wid, sending_button.objectName(), '',
                                                             QLineEdit.Normal, '')

            if ok_pressed and set_value != '':
                # Disconnect button signal and set active icon
                sending_button.pending_set()

                # Send request
                kat_client.send_request('?' + command_name + ' ' + str(set_value))

        except Exception as e:
            print(e)

    def _btn_options_click(self):
        """
        General slot for options button signal.
        :return:
        """
        try:
            # TODO: Export to generic dialog-based config menu
            sending_button = self.sender()
            client_name, sensor_name = sending_button.objectName().split(':')
            sensor = self._katcp_clients[client_name].sensor_map_get()[sensor_name]

            # Create the widget and choose what kind of dialog to show
            dialog_wid = QWidget()

            # Show options dialog
            set_value, ok_pressed = QInputDialog.getItem(dialog_wid, sending_button.objectName(),
                                                         'Options for: {}:{}'.format(client_name, sensor_name),
                                                         ['Graph', 'Clear History'], 0, False)

            if ok_pressed is True:
                if set_value == 'Graph':

                    # Check if sensor type is plotable
                    if sensor.sensor_type_float_int_check() is False:
                        MessageBox(QMessageBox.Warning,
                                   'Unsupported sensor type',
                                   'Plotting is currently only supported for float/int type sensors.',
                                   QMessageBox.Ok)
                        return

                    # Where to add the new plot
                    set_value, ok_pressed = QInputDialog.getItem(dialog_wid, sending_button.objectName(),
                                                                 'Add to Graph:',
                                                                 ['Add New Graph'] + [x for x in
                                                                                      self._graph_grid.widgets.keys()],
                                                                 0,
                                                                 False)
                    if ok_pressed is True:

                        # New graph
                        if set_value == 'Add New Graph':

                            # Graph title
                            set_value, ok_pressed = QInputDialog.getText(dialog_wid,
                                                                         'Graph-Name',
                                                                         'Enter Graph-Name (leave empty for auto):'
                                                                         )

                            if ok_pressed is True:
                                graph_title = str(set_value)

                                # Set a default title if none was provided
                                if graph_title == '':
                                    # Current number of graphs as start index for graph name
                                    graph_num = len(self._graph_grid.widgets)

                                    # Check if graph with name is present
                                    while True:
                                        graph_title = '{}_{}'.format('Graph', graph_num)
                                        graph_num += 1
                                        if graph_title not in self._graph_grid.widgets:
                                            break

                                # Check if graph is already
                                if graph_title in self._graph_grid.widgets:
                                    MessageBox(QMessageBox.Warning,
                                               'Graph title in use',
                                               'Graph \'{}\' already on Graph\'s view.'.format(graph_title),
                                               QMessageBox.Ok)
                                else:
                                    graph = Plotter(show_legend=True,
                                                    plots=[client_name + ':' + sensor_name],
                                                    label_x='time',
                                                    label_y='Y',
                                                    graph_title=graph_title,
                                                    show_grid=True,
                                                    background=Plotter.GraphColors.AUTO,
                                                    nav_buttons=['Remove Graph', 'Clear Data'])
                                    # Connect graph close button
                                    graph.nav_buttons['Remove Graph'].clicked.connect(self._btn_graph_remove_click)
                                    graph.nav_buttons['Clear Data'].clicked.connect(
                                        self._btn_sensor_history_clear_click)

                                    # Add new graph
                                    self._graph_grid.add_widget_auto(graph)

                                    # Activate graphs window
                                    self._show_graphs_window()

                        else:  # Add to existing graph
                            graph = self._graph_grid.widgets[set_value]
                            graph.add_plot(client_name + ':' + sensor_name)

                            # Activate graphs window
                            self._show_graphs_window()
                elif set_value == 'Clear History':
                    # Clear sensor's history data
                    self._sensor_history_clear(client_name, sensor_name)

        except Exception as e:
            self._logger.write_exception('Error: _btn_options_click')

    def _btn_graph_remove_click(self):
        """
        Slot for removing a grap from graphs view.
        :return:
        """
        sending_button = self.sender()
        button_name = sending_button.objectName()
        self._graph_grid.remove_widget(self._graph_grid.widgets[button_name])

    def _btn_sensor_history_clear_click(self):
        """
        Slot for clearing a sensor's history data.
        :return:
        """
        sending_button = self.sender()
        graph = self._graph_grid.widgets[sending_button.objectName()]

        for plot in graph.plot_refs.keys():
            # Extract names
            client_name, sensor_name = plot.split(':')

            # Clear history data
            self._sensor_history_clear(client_name, sensor_name)

    def _show_graphs_window(self):
        """
        Show and activate the graphs window.
        :return:
        """
        self._window_graph.bring_to_front()

    def _hide_sub_windows(self):
        """
        Hides all sub-windows.
        Routine can be called when main window closes to make sure,
        self.aboutToQuit fires (only if all windows hidden).
        :return:
        """
        self._window_graph.hide_window()

        for _, window_packetizer in self._windows_packetizers.items():
            window_packetizer.hide_window()

    def threads_stop(self):
        """
        Stops all active threads.
        Must be called on application shutdown.
        :return:
        """
        self.update_thread_table.stop_thread()
        self.update_thread_graphs.stop_thread()
