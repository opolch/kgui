import sys
from argparse import ArgumentParser
from epl.epl_katcp.epl_katcp_client import KatcpClient
from epl.epl_katcp.epl_katcp_packetizer import Packetizer
from epl.epl_string import epl_string_split
from epl.epl_logging.epl_logging_log import Log
from kGui_application import MainApplication
from configs import kGui_config
from epl.epl_qt.epl_qt_dialogs import MessageBox, QMessageBox
import time


def main():
    """
    The main routine.
    :return:
    """

    def shutdown_application():
        """
        Function to handle the application shutdown.
        Stop threads, disconnect clients...
        :return:
        """

        # Close sensor update threads
        window_application.threads_stop()

        # Stop all clients
        for client_name, client in katcp_clients.items():
            client.stop()
            logger_main.write_info('Client {} stopped.'.format(client_name))

        logger_main.write_info('Application will be shut down...')

    # Initializes the args module
    args = argument_parser()

    # Check args
    client_config, application_config, servers, log_level, sensor_history_depth = argument_check(args)

    # Instantiate the main application window
    window_application = MainApplication('kGui')

    # Check if config settings were passed from command line.
    # Otherwise, show selection dialog
    if client_config is None:
        # Choose config setting
        config_setting_name = window_application.server_config_select(kGui_config.client_opts)

        if config_setting_name is None:
            MessageBox(QMessageBox.Critical,
                       'Invalid configuration setting',
                       'You selected an invalid configuration setting.\nThe application will exit now!',
                       QMessageBox.Ok)
            exit()

        client_config = kGui_config.client_opts[config_setting_name]
        application_config = kGui_config.application_setups[config_setting_name]

        # Choose servers
        servers = window_application.server_select(client_config['server'])

        # Check if user selected valid server(s)
        if servers is None:
            MessageBox(QMessageBox.Critical,
                       'Invalid server configuration',
                       'You selected an invalid server configuration.\nThe application will exit now!',
                       QMessageBox.Ok)
            exit()

    # Initialize the loggers
    logger_main = Log(name='kGui.main',
                      level_console=Log.LoggingLevels[log_level.upper()],
                      formatter_console=[Log.Formatters.ASC_TIME, Log.Formatters.FILE_NAME,
                                         Log.Formatters.FUNC_NAME, Log.Formatters.LINE_NUMBER],
                      )
    logger_katcp = Log(name='kGui.katcp',
                       level_console=Log.LoggingLevels[log_level.upper()],
                       formatter_console=[Log.Formatters.ASC_TIME, Log.Formatters.FILE_NAME,
                                          Log.Formatters.FUNC_NAME, Log.Formatters.LINE_NUMBER],
                       )

    # Setup katcp client(s)
    server_counter = 0
    katcp_clients = dict()
    for value in client_config['server']:

        # We support name/host pairs in a config entry.
        names_display = epl_string_split.split_always_list(str(servers[server_counter][0]), ',')
        hosts = epl_string_split.split_always_list(str(servers[server_counter][1]), ',')
        ports = epl_string_split.split_always_list(str(servers[server_counter][2]), ',')
        katcp_roots = epl_string_split.split_always_list(str(servers[server_counter][3]), ',')
        device_type, sensor_list, command_list = value[1:]
        device_type = device_type.upper()

        # Setup the sensor_init dict with sampling strategy and params (not in config yet)
        update_sensors = dict()
        for update_sensor in sensor_list:
            update_sensors[update_sensor[0]] = {'strategy': update_sensor[1], 'params': update_sensor[2]}

        # Iterate over name/host pairs (in case) and init servers
        for (name, host, port, katcp_root) in zip(names_display, hosts, ports, katcp_roots):
            if device_type == KatcpClient.DeviceType.GENERIC_KATCP.name:  # Standard katcp-device
                # Init a generic katcp client
                katcp_client = KatcpClient(device_host=host,
                                           device_port=port,
                                           device_name=name,
                                           katcp_root_name=katcp_root,
                                           connect_timeout=5,
                                           init_timeout=float(client_config['timeout_init']),
                                           update_sensors_dict=update_sensors,
                                           command_list=command_list,
                                           auto_reconnect=True,
                                           sensor_period_default=5,
                                           logger=logger_katcp,
                                           sensor_hist_depth=sensor_history_depth)
            elif device_type == KatcpClient.DeviceType.PACKETIZER.name:
                # Init a packetizer client
                katcp_client = Packetizer(device_host=host,
                                          device_port=port,
                                          device_name=name,
                                          katcp_root_name=katcp_root,
                                          connect_timeout=5,
                                          init_timeout=float(client_config['timeout_init']),
                                          update_sensors_dict=update_sensors,
                                          command_list=command_list,
                                          auto_reconnect=True,
                                          refresh_interval_s=2,
                                          logger=logger_katcp,
                                          number_data_consumers=2,
                                          sensor_hist_depth=sensor_history_depth,
                                          spectra_enable_on_connect=True,
                                          spectra_enable_verify=True)
            else:
                katcp_client = None
                logger_main.write_error("Device-Type not known: {}".format(device_type))
                shutdown_application()

            # Connect the client
            katcp_client.init_connect()
            katcp_clients[katcp_client.device_name] = katcp_client

            logger_main.write_info('Added {} client from client_config.'.format(katcp_client.device_name))
            logger_main.write_debug('Client parameters: {}:{}:{}:{}'.format(device_type, name, host, port))

            # Count up server counter
            server_counter += 1

    # Wait for all the clients to be initialized before starting the window application
    wait_init = 0
    while True:
        all_init = True

        # Iterate over clients
        for katcp_client in katcp_clients.values():
            if katcp_client.device_state_get(keep=True) != KatcpClient.DeviceStates.INITIALIZED:
                all_init = False
                break
        if all_init is True:
            break
        if wait_init > 20:
            break

        wait_init += 1
        time.sleep(1)

    # Setup main application
    window_application.init_main_window(katcp_clients, application_config, logger_main)
    window_application.quit_signal.connect(shutdown_application)

    # Setup special functions
    for _, client in katcp_clients.items():
        if client.device_type == KatcpClient.DeviceType.PACKETIZER:
            window_application.special_function_add(client)

    # Execute the application
    sys.exit(window_application.exec())


def argument_parser():
    """
    Setup the ArgumentParser module.
    :return: Parsed args as dict.
    """

    arg_pars = ArgumentParser()
    arg_pars.add_argument('-c', '--config-setting', required=False,
                          choices=[setup for setup in kGui_config.application_setups], metavar='config_setting',
                          help='One of the available configuration settings (also see -a option): {}'.format(
                              [setup for setup in kGui_config.application_setups]))
    arg_pars.add_argument('-s', '--server', required=False, action='store', metavar='disp-name:host:port:kcp-root-name', nargs='*',
                          help='disp-name:host:port:kcp-root-name combination of katcp-server to connect to '
                               '(multiple servers separated by space). '
                               'Number of servers must correspond to configuration setting (use -a for information).')
    arg_pars.add_argument('-a', '--available-config', required=False, action='store_const', const=1,
                          help='Returns a list of available configurations.')
    arg_pars.add_argument('-l', '--log-level', required=False,
                          choices=[level.name.lower() for level in Log.LoggingLevels], metavar='log_level',
                          default=Log.LoggingLevels.INFO.name.lower(),
                          help='Set one of the available logging levels {}.'.format(
                              [level.name.lower() for level in Log.LoggingLevels]))
    arg_pars.add_argument('-d', '--sensor-history-depth', required=False, metavar='sensor_history_depth',
                          type=int, action='store', default=256,
                          help='Number of history entries to store per sensor.')
    return vars(arg_pars.parse_args())


def argument_check(args):
    """
    Checks the parsed args for consistency and syntax errors.
    :param args: Parsed args.
    :return: client_config, application_config, servers, log_level.
    """
    servers = None
    client_config = None
    application_config = None

    if args['available_config'] is not None:
        print('Available configurations:')
        for key, value in kGui_config.client_opts.items():
            print('* {}, {}, override={}'.format(key, value['description'], value['override']))
            if value['override'].upper() == 'TRUE':  # Be generous against typos
                print('  --> expecting {} server(s) to be overwritten by -s option.'.format(len(value['server'])))
        exit()
    if args['config_setting'] is not None:
        try:
            client_config = kGui_config.client_opts[args['config_setting']]
            application_config = kGui_config.application_setups[args['config_setting']]
        except Exception as e:
            print('Configuration setting not valid (-c/--config-setting)!')
            exit()
        # Server ignored if no config setting is passed
        if args['server'] is not None:
            try:
                # Check if number of servers correspond to config setting
                if len(args['server']) != len(client_config['server']):
                    print('Number of servers must correspond to configuration setting (use -a for more information).')
                    exit()

                # Check if config settings can be overwritten
                if client_config['override'] == 'False':
                    print('Config setting not allowed to be overwritten (use -a for more information).')
                    exit()

                # Overwrite server parameters
                servers = list()
                for i in range(0, len(args['server'])):
                    servers.append(epl_string_split.split_always_list(args['server'][i], ':'))
            except Exception as e:
                print('Server strings are not in correct format (see -h for help).', e)
                exit()
        else:
            print('Please provide the server information (see -h for help).')
            exit()

    return client_config, application_config, servers, args['log_level'], int(args['sensor_history_depth'])


if __name__ == "__main__":
    main()
