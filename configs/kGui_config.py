client_opts = {
    'rsc':
        {
            'description': 'Generic setup for a rsc',
            'override': 'True',
            'timeout_init': '10',
            'server':
                [
                    [
                        {
                            'RSC000': ['RSC000', '10.96.0.20', 7147, 'rxs'],
                            'RSC002': ['RSC002', '10.96.2.20', 7147, 'rxs'],
                            'RSC003': ['RSC003', '10.96.3.20', 7147, 'rxs'],
                            'RSC004': ['RSC004', '10.96.4.20', 7147, 'rxs'],
                            'RSC005': ['RSC005', '10.96.5.20', 7147, 'rxs'],
                            'RSC006': ['RSC006', '10.96.6.20', 7147, 'rxs'],
                            'RSC007': ['RSC007', '10.96.7.20', 7147, 'rxs'],
                            'RSC008': ['RSC008', '10.96.8.20', 7147, 'rxs'],
                            'RSC011': ['RSC011', '10.96.11.20', 7147, 'rxs'],
                            'RSC021': ['RSC021', '10.96.21.20', 7147, 'rxs'],
                            'RSC030': ['RSC030', '10.96.30.20', 7147, 'rxs'],
                            'RSC043': ['RSC043', '10.96.43.20', 7147, 'rxs'],
                            'RSC045': ['RSC045', '10.96.45.20', 7147, 'rxs'],
                            'RSC049': ['RSC049', '10.96.49.20', 7147, 'rxs'],
                            'RSC053': ['RSC053', '10.96.53.20', 7147, 'rxs'],
                            'RSC056': ['RSC056', '10.96.56.20', 7147, 'rxs'],
                            'RSC057': ['RSC057', '10.96.57.20', 7147, 'rxs'],
                            'RSC060': ['RSC060', '10.96.60.20', 7147, 'rxs'],
                            'SPFC-S': ['SP-S', '10.96.64.102', 7157, 'rxs'],
                            'SPFC-KU': ['SP-KU', '10.96.64.102', 7147, 'rxs'],
                            'RSC@RX1': ['RX1', '134.104.21.64', 53121, 'rxs'],
                            'RSC@RX2': ['RX2', '134.104.21.61', 53121, 'rxs'],
                            'RSC@RX3': ['RX3', '134.104.21.127', 53121, 'rxs'],
                            'RSC@RFI': ['RFI', '134.104.21.106', 53121, 'rxs'],
                            'RSC@S110': ['S110', '134.104.71.32', 53121, 'rxs'],
                            'RSC@S45': ['S45', '134.104.71.33', 53121, 'rxs'],
                        },
                        'generic_katcp',  # Type
                        [
                            ['rxs.', 'differential', '0.01'],  # others differential
                        ],
                        [  # Command-list
                        ]
                    ],
                ],
        },
    'band5b':
        {
            'description': 'Band-5b rsc setup',
            'override': 'True',
            'timeout_init': '10',
            'server':
                [
                    [
                        {
                            'b5b@rx1(21.64)': ['5b@r1', '134.104.21.64', 53121, 'rx5b'],
                            'b5b@rx2(21.61)': ['5b@r2', '134.104.21.61', 53121, 'rx5b'],
                            'b5b@rx3(21.127)': ['5b@r3', '134.104.21.127', 53121, 'rx5b'],
                            'b5b@rfi(21.106)': ['5b@rfi', '134.104.21.106', 53121, 'rx5b'],
                            'b5b-test-bench-bn(22.48)': ['b5b', '134.104.22.48', 53121, 'rx5b'],
                        },
                        'generic_katcp',  # Type
                        [
                            ['rx5b.', 'differential', '0.01'],  # others differential
                        ],
                        [  # Command-list
                        ]
                    ],
                ],
        },
    'band5b-inaf':
        {
            'description': 'Band-5b rsc setup',
            'override': 'True',
            'timeout_init': '10',
            'server':
                [
                    [
                        {
                            'b5b@rx1(21.64)': ['5b@r1', '134.104.21.64', 53121, 'rx5b'],
                            'b5b@rx2(21.61)': ['b5b', '134.104.21.61', 53121, 'rx5b'],
                            'b5b@rx3(21.127)': ['5b@r3', '134.104.21.127', 53121, 'rx5b'],
                            'b5b@rfi(21.106)': ['5b@rfi', '134.104.21.106', 53121, 'rx5b'],
                            'b5b-test-bench-bn(22.48)': ['b5b', '134.104.22.48', 53121, 'rx5b'],
                        },
                        'generic_katcp',  # Type
                        [
                            ['rx5b.', 'differential', '0.01'],  # others differential
                        ],
                        [  # Command-list
                        ]
                    ],
                ],
        },
    'packetizer':
        {
            'description': 'Packetizer Version 1 (2GHz) and 2 (3GHz)',
            'override': 'True',
            'timeout_init': '10',
            'server':
                [
                    [
                        {
                            'PCK000': ['PCK000', '10.96.0.41', 7147, 'rxs'],
                            'PCK002': ['PCK002', '10.96.2.41', 7147, 'rxs'],
                            'PCK003': ['PCK003', '10.96.3.41', 7147, 'rxs'],
                            'PCK004': ['PCK004', '10.96.4.41', 7147, 'rxs'],
                            'PCK005': ['PCK005', '10.96.5.41', 7147, 'rxs'],
                            'PCK006': ['PCK006', '10.96.6.41', 7147, 'rxs'],
                            'PCK007': ['PCK007', '10.96.7.41', 7147, 'rxs'],
                            'PCK008': ['PCK008', '10.96.8.41', 7147, 'rxs'],
                            'PCK011': ['PCK011', '10.96.11.41', 7147, 'rxs'],
                            'PCK021': ['PCK021', '10.96.21.41', 7147, 'rxs'],
                            'PCK030': ['PCK030', '10.96.30.41', 7147, 'rxs'],
                            'PCK043': ['PCK043', '10.96.43.41', 7147, 'rxs'],
                            'PCK045': ['PCK045', '10.96.45.41', 7147, 'rxs'],
                            'PCK049': ['PCK049', '10.96.49.41', 7147, 'rxs'],
                            'PCK053': ['PCK053', '10.96.53.41', 7147, 'rxs'],
                            'PCK056': ['PCK056', '10.96.56.41', 7147, 'rxs'],
                            'PCK057': ['PCK057', '10.96.57.41', 7147, 'rxs'],
                            'PCK060': ['PCK060', '10.96.60.41', 7147, 'rxs'],
                            'PCK_MPI_S': ['PCK-S', '10.96.64.41', 7147, 'rxs'],
                            'PCK_MPI_KU': ['PCK-KU', '10.96.64.42', 7147, 'rx'],
                            'PCK@RX1': ['PCK1', '134.104.21.130', 7147, 'rxs'],
                            'PCK@RX2': ['PCK2', '134.104.21.129', 7147, 'rxs'],
                            'PCK@RX3': ['PCK3', '134.104.21.91', 7147, 'rxs'],
                            'PCK@RX4': ['PCK4', '134.104.26.213', 7147, 'rxs'],
                            'pck_3G_pfc_bn': ['P3G', '134.104.21.157', 7147, 'edd'],
                            'pck_3G_pfc_eff': ['P3G', '134.104.78.64', 7147, 'edd'],
                            'pck_2G_pfc_eff': ['P2G', '134.104.73.149', 7147, 'rxs'],
                            'pck_s110_sfc': ['110', '134.104.73.152', 7147, 'rxs'],
                            'pck_s45_sfc': ['45', '134.104.73.150', 7147, 'rxs'],
                        },
                        'packetizer',  # Type
                        [
                            ['.', 'differential', '0.1'],  # others differential
                        ],
                        [  # Command-list
                        ]
                    ],
                ],
        },
    # 'rsc_pck':
    #     {
    #         'description': 'Generic setup for a rsc/packetizer combination',
    #         'override': 'True',
    #         'timeout_init': '10',
    #         'server':
    #             [
    #                 [
    #                     {
    #                         'RSC000': ['RSC000', '10.96.0.20', 7147, 'rxs'],
    #                         'RSC002': ['RSC002', '10.96.2.20', 7147, 'rxs'],
    #                         'RSC003': ['RSC003', '10.96.3.20', 7147, 'rxs'],
    #                         'RSC004': ['RSC004', '10.96.4.20', 7147, 'rxs'],
    #                         'RSC005': ['RSC005', '10.96.5.20', 7147, 'rxs'],
    #                         'RSC006': ['RSC006', '10.96.6.20', 7147, 'rxs'],
    #                         'RSC007': ['RSC007', '10.96.7.20', 7147, 'rxs'],
    #                         'RSC008': ['RSC008', '10.96.8.20', 7147, 'rxs'],
    #                         'RSC011': ['RSC011', '10.96.11.20', 7147, 'rxs'],
    #                         'RSC021': ['RSC021', '10.96.21.20', 7147, 'rxs'],
    #                         'RSC030': ['RSC030', '10.96.30.20', 7147, 'rxs'],
    #                         'RSC043': ['RSC043', '10.96.43.20', 7147, 'rxs'],
    #                         'RSC045': ['RSC045', '10.96.45.20', 7147, 'rxs'],
    #                         'RSC049': ['RSC049', '10.96.49.20', 7147, 'rxs'],
    #                         'RSC053': ['RSC053', '10.96.53.20', 7147, 'rxs'],
    #                         'RSC056': ['RSC056', '10.96.56.20', 7147, 'rxs'],
    #                         'RSC057': ['RSC057', '10.96.57.20', 7147, 'rxs'],
    #                         'RSC060': ['RSC060', '10.96.60.20', 7147, 'rxs'],
    #                         'SPFC-S': ['SP-S', '10.96.64.102', 7157, 'rxs'],
    #                         'SPFC-KU': ['SP-KU', '10.96.64.102', 7147, 'rxs'],
    #                         'RSC@RX1': ['RX1', '134.104.21.64', 53121, 'rxs'],
    #                         'RSC@RX2': ['RX2', '134.104.21.61', 53121, 'rxs'],
    #                         'RSC@RX3': ['RX3', '134.104.21.127', 53121, 'rxs'],
    #                         'RSC@RFI': ['RFI', '134.104.21.106', 53121, 'rxs'],
    #                         'RSC@S110': ['S110', '134.104.71.32', 53121, 'rxs'],
    #                         'RSC@S45': ['S45', '134.104.71.33', 53121, 'rxs'],
    #                     },
    #                     'generic_katcp',  # Type
    #                     [
    #                         ['rxs.', 'differential', '0.05'],  # others differential
    #                     ],
    #                     [  # Command-list
    #                     ]
    #                 ],
    #                 [
    #                     {
    #                         'PCK000': ['PCK000', '10.96.0.41', 7147, 'rxs'],
    #                         'PCK002': ['PCK002', '10.96.2.41', 7147, 'rxs'],
    #                         'PCK003': ['PCK003', '10.96.3.41', 7147, 'rxs'],
    #                         'PCK004': ['PCK004', '10.96.4.41', 7147, 'rxs'],
    #                         'PCK005': ['PCK005', '10.96.5.41', 7147, 'rxs'],
    #                         'PCK006': ['PCK006', '10.96.6.41', 7147, 'rxs'],
    #                         'PCK007': ['PCK007', '10.96.7.41', 7147, 'rxs'],
    #                         'PCK008': ['PCK008', '10.96.8.41', 7147, 'rxs'],
    #                         'PCK011': ['PCK011', '10.96.11.41', 7147, 'rxs'],
    #                         'PCK021': ['PCK021', '10.96.21.41', 7147, 'rxs'],
    #                         'PCK030': ['PCK030', '10.96.30.41', 7147, 'rxs'],
    #                         'PCK043': ['PCK043', '10.96.43.41', 7147, 'rxs'],
    #                         'PCK045': ['PCK045', '10.96.45.41', 7147, 'rxs'],
    #                         'PCK049': ['PCK049', '10.96.49.41', 7147, 'rxs'],
    #                         'PCK053': ['PCK053', '10.96.53.41', 7147, 'rxs'],
    #                         'PCK056': ['PCK056', '10.96.56.41', 7147, 'rxs'],
    #                         'PCK057': ['PCK057', '10.96.57.41', 7147, 'rxs'],
    #                         'PCK060': ['PCK060', '10.96.60.41', 7147, 'rxs'],
    #                         'PCK_MPI_S': ['PCK-S', '10.96.64.41', 7147, 'rxs'],
    #                         'PCK_MPI_KU': ['PCK-KU', '10.96.64.42', 7147, 'rxs'],
    #                         'PCK@RX1': ['PCK1', '134.104.21.130', 7147, 'rxs'],
    #                         'PCK@RX2': ['PCK2', '134.104.21.129', 7147, 'rxs'],
    #                         'PCK@RX3': ['PCK3', '134.104.21.91', 7147, 'rxs'],
    #                         'PCK@RX4': ['PCK4', '134.104.26.213', 7147, 'rxs'],
    #                         'PCK@S110': ['PCK110', '134.104.73.152', 7147, 'rxs'],
    #                         'PCK@S45': ['PCK45', '134.104.73.150', 7147, 'rxs'],
    #                     },
    #                     'packetizer',  # Type
    #                     [
    #                         ['rxs.', 'differential', '1'],  # others differential
    #                         ['s-band.scg.time.stamps.modulus', 'event', ''],
    #                     ],
    #                     [  # Command-list
    #                     ]
    #                 ],
    #             ],
    #     },
    'rsc_pck_v2':
        {
            'description': 'Generic setup for a rsc/packetizer combination',
            'override': 'True',
            'timeout_init': '20',
            'server':
                [
                    [
                        {
                            'RSC000': ['RSC000', '10.96.0.20', 7147, 'rxs'],
                            'RSC002': ['RSC002', '10.96.2.20', 7147, 'rxs'],
                            'RSC003': ['RSC003', '10.96.3.20', 7147, 'rxs'],
                            'RSC004': ['RSC004', '10.96.4.20', 7147, 'rxs'],
                            'RSC005': ['RSC005', '10.96.5.20', 7147, 'rxs'],
                            'RSC006': ['RSC006', '10.96.6.20', 7147, 'rxs'],
                            'RSC007': ['RSC007', '10.96.7.20', 7147, 'rxs'],
                            'RSC008': ['RSC008', '10.96.8.20', 7147, 'rxs'],
                            'RSC011': ['RSC011', '10.96.11.20', 7147, 'rxs'],
                            'RSC021': ['RSC021', '10.96.21.20', 7147, 'rxs'],
                            'RSC030': ['RSC030', '10.96.30.20', 7147, 'rxs'],
                            'RSC043': ['RSC043', '10.96.43.20', 7147, 'rxs'],
                            'RSC045': ['RSC045', '10.96.45.20', 7147, 'rxs'],
                            'RSC049': ['RSC049', '10.96.49.20', 7147, 'rxs'],
                            'RSC053': ['RSC053', '10.96.53.20', 7147, 'rxs'],
                            'RSC056': ['RSC056', '10.96.56.20', 7147, 'rxs'],
                            'RSC057': ['RSC057', '10.96.57.20', 7147, 'rxs'],
                            'RSC060': ['RSC060', '10.96.60.20', 7147, 'rxs'],
                            'SPFC-S': ['SP-S', '10.96.64.102', 7157, 'rxs'],
                            'SPFC-KU': ['SP-KU', '10.96.64.102', 7147, 'rxs'],
                            'SPFC-ME119': ['SP-119', '10.96.66.102', 7147, 'rxs'],
                            'RSC@RX1': ['RX1', '134.104.21.64', 53121, 'rxs'],
                            'RSC@RX2': ['RX2', '134.104.21.61', 53121, 'rxs'],
                            'RSC@RX3': ['RX3', '134.104.21.127', 53121, 'rxs'],
                            'RSC@RFI': ['RFI', '134.104.21.106', 53121, 'rxs'],
                            'RSC@S110': ['S110', '134.104.71.32', 53121, 'rxs'],
                            'RSC@S45': ['S45', '134.104.71.33', 53121, 'rxs'],
                        },
                        'generic_katcp',  # Type
                        [
                            ['rxs.', 'differential', '0.001'],  # others differential
                        ],
                        [  # Command-list
                        ]
                    ],
                    [
                        {
                            'PCK000': ['PCK000', '10.96.0.41', 7147, 'rxs'],
                            'PCK002': ['PCK002', '10.96.2.41', 7147, 'rxs'],
                            'PCK003': ['PCK003', '10.96.3.41', 7147, 'rxs'],
                            'PCK004': ['PCK004', '10.96.4.41', 7147, 'rxs'],
                            'PCK005': ['PCK005', '10.96.5.41', 7147, 'rxs'],
                            'PCK006': ['PCK006', '10.96.6.41', 7147, 'rxs'],
                            'PCK007': ['PCK007', '10.96.7.41', 7147, 'rxs'],
                            'PCK008': ['PCK008', '10.96.8.41', 7147, 'rxs'],
                            'PCK011': ['PCK011', '10.96.11.41', 7147, 'rxs'],
                            'PCK021': ['PCK021', '10.96.21.41', 7147, 'rxs'],
                            'PCK030': ['PCK030', '10.96.30.41', 7147, 'rxs'],
                            'PCK043': ['PCK043', '10.96.43.41', 7147, 'rxs'],
                            'PCK045': ['PCK045', '10.96.45.41', 7147, 'rxs'],
                            'PCK049': ['PCK049', '10.96.49.41', 7147, 'rxs'],
                            'PCK053': ['PCK053', '10.96.53.41', 7147, 'rxs'],
                            'PCK056': ['PCK056', '10.96.56.41', 7147, 'rxs'],
                            'PCK057': ['PCK057', '10.96.57.41', 7147, 'rxs'],
                            'PCK060': ['PCK060', '10.96.60.41', 7147, 'rxs'],
                            'PCK_MPI_S': ['PCK-S', '10.96.64.41', 7147, 'rxs'],
                            'PCK_MPI_KU': ['PCK-KU', '10.96.64.42', 7147, 'rxs'],
                            'PCK_ME119': ['PCK-119', '10.96.66.41', 7147, 'rxs'],
                            'PCK@RX1': ['PCK1', '134.104.21.130', 7147, 'rxs'],
                            'PCK@RX2': ['PCK2', '134.104.21.129', 7147, 'rxs'],
                            'PCK@RX3': ['PCK3', '134.104.21.91', 7147, 'rxs'],
                            'PCK@RX4': ['PCK4', '134.104.26.213', 7147, 'rxs'],
                            'PCK@S110': ['PCK110', '134.104.73.152', 7147, 'rxs'],
                            'PCK@S45': ['PCK45', '134.104.73.150', 7147, 'rxs'],
                        },
                        'packetizer',  # Type
                        [
                            ['.', 'differential', '0.1'],  # others differential
                        ],
                        [  # Command-list
                        ]
                    ],
                ],
        },
    'edd_rcm':
        {
            'description': 'edd frontend controller',
            'override': 'True',
            'timeout_init': '20',
            'server':
                [
                    [
                        {
                            'ubb': ['ubb', '134.104.26.225', 7147, 'rxs'],
                        },
                        'generic_katcp',  # Type
                        [
                            ['rxs.', 'event_rate', '5 30'],  # event-rate sampling
                        ],
                        [  # Command-list
                        ]
                    ],
                ],
        },
    'edd_rcm_ubb':
        {
            'description': 'edd frontend controller for the new UBB',
            'override': 'True',
            'timeout_init': '20',
            'server':
                [
                    [
                        {
                            'ubb-bn': ['ubb', '134.104.26.225', 7147, 'rxubb'],
                            'ubb-eff': ['ubb', '134.104.71.36', 7147, 'rxubb'],
                        },
                        'generic_katcp',  # Type
                        [
                            ['rxubb.', 'event_rate', '5 30'],  # event-rate sampling
                        ],
                        [  # Command-list
                        ]
                    ],
                ],
        },
    'mkat_rsc_states':
        {
            'description': 'States of all s-band rscs@meerkat',
            'override': 'False',
            'timeout_init': '10',
            'server':
                [
                    [
                        {
                            'M000-M060':
                                [
                                    'M000, M002, M003, M004, M005, M006, M007, M008, M011, M021, M030, M043, M045, M049, M053, M056, M057, M060',
                                    '10.96.0.20, 10.96.2.20, 10.96.3.20, 10.96.4.20, 10.96.5.20, 10.96.6.20, 10.96.7.20, 10.96.8.20, 10.96.11.20, '
                                    '10.96.21.20, 10.96.30.20, 10.96.43.20, 10.96.45.20, 10.96.49.20, 10.96.53.20, 10.96.56.20, 10.96.57.20, 10.96.60.20',
                                    '7147, 7147, 7147, 7147, 7147, 7147, 7147, 7147, 7147, 7147, 7147, 7147, 7147, 7147, 7147, 7147, 7147, 7147'
                                ]
                        },
                        'generic_katcp',  # Type
                        [
                            ['rxs.state', 'event', ''],
                            ['rxs.display-errors', 'event', ''],
                        ],
                        [  # Command-list
                        ]
                    ],
                ],
        },
    'mkat_pck_states':
        {
            'description': 'States of all s-band packetizers@meerkat',
            'override': 'False',
            'timeout_init': '10',
            'server':
                [
                    [
                        {
                            'M000-M060':
                                [
                                    'M000, M002, M003, M004, M005, M006, M007, M008, M011, M021, M030, M043, M045, M049, M053, M056, M057, M060',
                                    '10.96.0.41, 10.96.2.41, 10.96.3.41, 10.96.4.41, 10.96.5.41, 10.96.6.41, 10.96.7.41, 10.96.8.41, 10.96.11.41, '
                                    '10.96.21.41, 10.96.30.41, 10.96.43.41, 10.96.45.41, 10.96.49.41, 10.96.53.41, 10.96.56.41, 10.96.57.41, 10.96.60.41',
                                    '7147, 7147, 7147, 7147, 7147, 7147, 7147, 7147, 7147, 7147, 7147, 7147, 7147, 7147, 7147, 7147, 7147, 7147'
                                ]
                        },
                        'packetizer',  # Type
                        [
                            ['rxs.packetizer.1pps.length', 'event', ''],
                            ['rxs.packetizer.1pps.last', 'event', ''],
                            ['rxs.packetizer.freq.refclk1', 'event', ''],
                        ],
                        [  # Command-list
                        ]
                    ],
                ],
        },
    'tfrgen_full':
        {
            'description': 'Fully equipped TFR-Generator slide-in (Control+8TX)',
            'override': 'True',
            'timeout_init': '60',
            'server':
                [
                    [
                        {
                            'TFRBN': ['TFRBN', 'localhost', 1235, 'tfrgen'],
                            'TFRSA': ['TFRSA', 'localhost', 55125, 's-band-gen'],
                        },
                        'generic_katcp',
                        [
                            ['s-band-gen.', 'differential_rate', '0.01 10.0 120.0'],  # others differential
                        ],
                        [  # Command-list
                        ]
                    ],
                ],
        },
    'tfrgen_eff':
        {
            'description': 'TFR-Generator in Effelsberg configuration',
            'override': 'True',
            'timeout_init': '60',
            'server':
                [
                    [
                        {
                            'TFREFF': ['TFREFF', '134.104.71.31', 53122, 'tfr'],
                        },
                        'generic_katcp',
                        [
                            ['tfrgen.', 'differential_rate', '0.05 5.0 120.0'],  # others differential
                        ],
                        [  # Command-list
                        ]
                    ],
                ],
        },
    'ept@eff':
        {
            'description': 'Devices part of EPT',
            'override': 'False',
            'timeout_init': '10',
            'server':
                [
                    [
                        {
                            'EPT':
                                [
                                    'tic_round_trip_focus, tic_lant1_i3000, tic_lant1_lant2, tic_i3000_i45_pps, '
                                    'tic_lant1_i45, tic_i3000_i45_100m',
                                    '134.104.71.27, 134.104.71.27, 134.104.71.27, 134.104.71.27, 134.104.71.27, '
                                    '134.104.71.27',
                                    '53125, 53193, 53194, 53195, 53196, 53198'
                                ],
                        },
                        'generic_katcp',
                        [
                            ['tic', 'differential-rate', '0.0000000001 30.0 60.0'],  # others differential
                        ],
                        [  # Command-list
                        ]
                    ],
                ],
        },
    'intercom':
        {
            'description': 'InteRCoM devices',
            'override': 'True',
            'timeout_init': '10',
            'server':
                [
                    [
                        {
                            'MULTI-CBE': ['MULT-CBE', '134.104.71.27', 53320],
                        },
                        'generic_katcp',
                        [
                            ['multicbe.', 'event', ''],  # others event
                        ],
                        [  # Command-list
                        ]
                    ],
                ],
        },
}

application_setups = {
    'rsc':
        {
            'RSC':  # Clipboard_0
                {
                    'c1':  # col 1
                        {
                            'STATES':
                                [
                                    [
                                        '0',
                                        'rxs.expected-online',
                                        'expected-online',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.state',
                                        'state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.display-errors',
                                        'errors',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.startup-state',
                                        'startup-state',
                                        'rxs-startup-state',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.rsm.opmode',
                                        'operation mode',
                                        'rxs-rsm-opmode',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'STATEMACHINE':
                                [
                                    [
                                        '0',
                                        'rxs.rsm.statemachinestate',
                                        'sm-state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.rsm.tout-interval',
                                        'tout-interval',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.rsm.tout-counter',
                                        'tout-counter',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.rsm.uptime-counter',
                                        'uptime',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LIBRXS':
                                [
                                    [
                                        '0',
                                        'rxs.librxs.systemstate',
                                        'libRxS state',
                                        'rxs-librxs-switchstate',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.librxs.startupstate',
                                        'libRxS startup-state',
                                        'rxs-librxs-startupstate',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'DIGITIZER':
                                [
                                    [
                                        '0',
                                        'rxs.power.digitizer',
                                        'power',
                                        'rxs-power-digitizer',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'REFERENCE':
                                [
                                    [
                                        '0',
                                        'rxs.tfr.level1pps',
                                        '1pps',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.tfr.level100mhz',
                                        '100m',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'DOWNCONV':
                                [
                                    [
                                        '0',
                                        'rxs.dwnc.frequency',
                                        'freq',
                                        'rxs-dwnc-frequency',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.dwnc.lock',
                                        'lock',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'SYSTEM':
                                [
                                    [
                                        '0',
                                        '',
                                        '',
                                        'rxs-system-eepromall',
                                        'write-eeprom',
                                        'cell'
                                    ],
                                ],
                            'TANGO':
                                [
                                    [
                                        '0',
                                        'rxs.translator.rsm.vacrequest',
                                        'vac-req',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.translator.rsm.herequest',
                                        'he-req',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'c2':
                        {
                            'VALVE':
                                [
                                    [
                                        '0',
                                        'rxs.vac-valve-open',
                                        'valve',
                                        'rxs-cryocooler-vacuumvalve-openclose',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.cryocooler.v24',
                                        'V24',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.cryocooler.vvalve',
                                        'V-Valve',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.cryocooler.ivalve',
                                        'I-Valve',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'CRYO':
                                [
                                    [
                                        '0',
                                        'rxs.cryocooler.motorspeedmeasure',
                                        'motor',
                                        'rxs-cryocooler-motorstartspeed',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '',
                                        '',
                                        'rxs-cryocooler-motorreinit',
                                        'motor-stop',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.cryocooler.v30',
                                        'V30',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'DEWAR':
                                [
                                    [
                                        '0',
                                        'rxs.tempvac.temp15k',
                                        'T15',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.tempvac.temp70k',
                                        'T70',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.tempvac.vacuumdewar',
                                        'P-dewar',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.tempvac.vacuumsystem',
                                        'P-system',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.dewar.heater20k',
                                        'Heater20K',
                                        'rxs-dewar-heater20k',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.dewar.heater70k',
                                        'Heater70K',
                                        'rxs-dewar-heater70k',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'BOX-WEATHER':
                                [
                                    [
                                        '0',
                                        'rxs.boxweather.tempcold',
                                        'T-cold',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.boxweather.tempwarm',
                                        'T-warm',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.boxweather.hum01.humidity',
                                        'Hum01',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.boxweather.hum02.humidity',
                                        'Hum02',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'ATC':
                                [
                                    [
                                        '0',
                                        'rxs.atc.activate',
                                        'enabled',
                                        'rxs-atc-activate',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.tempisstable',
                                        'stable',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.setpoint',
                                        'setpoint',
                                        'rxs-atc-setpoint',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.objecttemp',
                                        'T-obj',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.sinktemp',
                                        'T-sink',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.outputcurrent',
                                        'I-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.outputvoltage',
                                        'V-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.supplyvoltage',
                                        'V-sup',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.errornumber',
                                        'error',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'c3':
                        {
                            'CHAIN-A':
                                [
                                    [
                                        '0',
                                        'rxs.mmic.enable.mmic1',
                                        'enabled',
                                        'rxs-mmic-enable-mmic1',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'SIG-PROC':
                                [
                                    [
                                        '0',
                                        'rxs.signalprocessors.sp01.attenuation',
                                        'attenuation',
                                        'rxs-signalprocessors-sp01-attenuation',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.signalprocessors.sp01.totalpower',
                                        'total-power',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-1':
                                [
                                    [
                                        '0',
                                        'rxs.mmic.drain01.drainvoltage',
                                        'Vd',
                                        'rxs-mmic-drain01-drainvoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.drain01.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.drain01.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.gate01.gatevoltage',
                                        'Vg',
                                        'rxs-mmic-gate01-gatevoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.gate01.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-2':
                                [
                                    [
                                        '0',
                                        'rxs.mmic.drain02.drainvoltage',
                                        'Vd',
                                        'rxs-mmic-drain02-drainvoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.drain02.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.drain02.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.gate02.gatevoltage',
                                        'Vg',
                                        'rxs-mmic-gate02-gatevoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.gate02.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-3':
                                [
                                    [
                                        '0',
                                        'rxs.mmic.drain03.drainvoltage',
                                        'Vd',
                                        'rxs-mmic-drain03-drainvoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.drain03.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.drain03.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.gate03.gatevoltage',
                                        'Vg',
                                        'rxs-mmic-gate03-gatevoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.gate03.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'c4':
                        {
                            'CHAIN-B':
                                [
                                    [
                                        '0',
                                        'rxs.mmic.enable.mmic2',
                                        'enabled',
                                        'rxs-mmic-enable-mmic2',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'SIG-PROC':
                                [
                                    [
                                        '0',
                                        'rxs.signalprocessors.sp02.attenuation',
                                        'attenuation',
                                        'rxs-signalprocessors-sp02-attenuation',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.signalprocessors.sp02.totalpower',
                                        'total-power',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-1':
                                [
                                    [
                                        '0',
                                        'rxs.mmic.drain04.drainvoltage',
                                        'Vd',
                                        'rxs-mmic-drain04-drainvoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.drain04.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.drain04.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.gate04.gatevoltage',
                                        'Vg',
                                        'rxs-mmic-gate04-gatevoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.gate04.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-2':
                                [
                                    [
                                        '0',
                                        'rxs.mmic.drain05.drainvoltage',
                                        'Vd',
                                        'rxs-mmic-drain05-drainvoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.drain05.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.drain05.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.gate05.gatevoltage',
                                        'Vg',
                                        'rxs-mmic-gate05-gatevoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.gate05.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-3':
                                [
                                    [
                                        '0',
                                        'rxs.mmic.drain06.drainvoltage',
                                        'Vd',
                                        'rxs-mmic-drain06-drainvoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.drain06.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.drain06.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.gate06.gatevoltage',
                                        'Vg',
                                        'rxs-mmic-gate06-gatevoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.gate06.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                },
        },
    'band5b':
        {
            'RSC':  # Clipboard_0
                {
                    'c1':  # col 1
                        {
                            'STATES':
                                [
                                    [
                                        '0',
                                        '.expected-online',
                                        'expected-online',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    # [
                                    #     '0',
                                    #     '.state',
                                    #     'state',
                                    #     '',
                                    #     '',
                                    #     'cell'
                                    # ],
                                    [
                                        '0',
                                        '.display-errors',
                                        'errors',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.startup-state',
                                        'startup-state',
                                        'rx5b-startup-state',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.rsm.opmode',
                                        'operation-mode',
                                        'rx5b-rsm-opmode',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'STATEMACHINE':
                                [
                                    [
                                        '0',
                                        '.rsm.debugmode',
                                        'debug-mode',
                                        'rx5b-rsm-debugmode',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.rsm.statemachinestate',
                                        'sm-state',
                                        'rx5b-rsm-statemachinestate',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.rsm.tout-interval',
                                        'tout-interval',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.rsm.tout-counter',
                                        'tout-counter',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.rsm.uptime-counter',
                                        'uptime',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LIBRX5B':
                                [
                                    [
                                        '0',
                                        '.librxs.systemstate',
                                        'libRxS state',
                                        'rx5b-librxs-switchstate',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.librxs.startupstate',
                                        'libRxS startup-state',
                                        'rx5b-librxs-startupstate',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'SYSTEM':
                                [
                                    [
                                        '0',
                                        '',
                                        '',
                                        '-system-eepromall',
                                        'write-eeprom',
                                        'cell'
                                    ],
                                ],
                        },
                    'c2':
                        {
                            'VALVE':
                                [
                                    [
                                        '0',
                                        '.vac.valve',
                                        'valve',
                                        'rx5b-vac-valve',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.cryocooler.v24',
                                        'V24',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.cryocooler.vvalve',
                                        'V-Valve',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.cryocooler.ivalve',
                                        'I-Valve',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'CRYO':
                                [
                                    [
                                        '0',
                                        '.cryocooler.motorspeedmeasure',
                                        'motor',
                                        'rx5b-cryocooler-motorstartspeed',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '',
                                        '',
                                        'rx5b-cryocooler-motorreinit',
                                        'motor-stop',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.cryocooler.v30',
                                        'V30',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'DEWAR':
                                [
                                    [
                                        '0',
                                        '.tempvac.temp.stage1',
                                        'T-15',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.tempvac.temp.pfs',
                                        'T-PFS',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.tempvac.temp.stage2',
                                        'T-70',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.tempvac.temp.rfs',
                                        'T-RFS',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.tempvac.vacuumdewar',
                                        'P-dewar',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.tempvac.vacuumsystem',
                                        'P-system',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.dewar.heater',
                                        'Heater',
                                        'rx5b-dewar-heater',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'BOX-WEATHER':
                                [
                                    [
                                        '0',
                                        '.boxweather.hum01.humidity',
                                        'Hum01',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.boxweather.hum02.humidity',
                                        'Hum02',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'c3':
                        {
                            'ATC':
                                [
                                    [
                                        '0',
                                        '.atc.activate',
                                        'enabled',
                                        'rx5b-atc-activate',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.atc.state',
                                        'state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.atc.setpoint',
                                        'setpoint',
                                        'rx5b-atc-setpoint',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.atc.objecttemp',
                                        'T-obj',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.atc.dutydebug',
                                        'Duty',
                                        'rx5b-atc-dutydebug',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.atc.pidvalp',
                                        'Val-P',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.atc.pidvali',
                                        'Val-I',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.atc.pidvald',
                                        'Val-D',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'PSU':
                                [
                                    [
                                        '0',
                                        '.psu.volt.p3v3rcm',
                                        'p3v3rcm',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.volt.p3v3ana',
                                        'p3v3ana',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.volt.p3v3dig',
                                        'p3v3dig',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.volt.p6',
                                        'p6',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.volt.p6dew',
                                        'p6dew',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.volt.n7',
                                        'n7',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.volt.p24',
                                        'p24',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.volt.p30',
                                        'p30',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.cur.p3v3rcm',
                                        'p3v3rcm',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.cur.p3v3ana',
                                        'p3v3ana',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.cur.p3v3dig',
                                        'p3v3dig',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.cur.p6',
                                        'p6',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.cur.p6dew',
                                        'p6dew',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.cur.n7',
                                        'n7',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.cur.p24',
                                        'p24',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.cur.p30',
                                        'p30',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ]
                        },
                    'c4':
                        {
                            'CHAIN-A':
                                [
                                    [
                                        '0',
                                        '.mmic.enable.mmic1',
                                        'enabled',
                                        'rx5b-mmic-enable-mmic1',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-1':
                                [
                                    [
                                        '0',
                                        '.mmic.drain01.drainvoltage',
                                        'Vd',
                                        'rx5b-mmic-drain01-drainvoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.mmic.drain01.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.mmic.drain01.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.mmic.gate01.gatevoltage',
                                        'Vg',
                                        'rx5b-mmic-gate01-gatevoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.mmic.gate01.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'RF-POWER':
                                [
                                    [
                                        '0',
                                        '.power.rfs',
                                        'Power',
                                        'rx5b-power-rfs',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'SIG-PROC-A':
                                [
                                    [
                                        '0',
                                        '.signalprocessors.sp01.attenuation',
                                        'attenuation',
                                        'rx5b-signalprocessors-sp01-attenuation',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'NOISE':
                                [
                                    [
                                        '0',
                                        '.noise.attenuation',
                                        'Att',
                                        'rx5b-noise-attenuation',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.noise.monitor',
                                        'Monit',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'c5':
                        {
                            'CHAIN-B':
                                [
                                    [
                                        '0',
                                        '.mmic.enable.mmic2',
                                        'enabled',
                                        'rx5b-mmic-enable-mmic2',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-1':
                                [
                                    [
                                        '0',
                                        '.mmic.drain02.drainvoltage',
                                        'Vd',
                                        'rx5b-mmic-drain02-drainvoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.mmic.drain02.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.mmic.drain02.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.mmic.gate02.gatevoltage',
                                        'Vg',
                                        'rx5b-mmic-gate02-gatevoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.mmic.gate02.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            '':  # dummy placeholder
                                [
                                    [
                                        '0',
                                        '',
                                        '',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'SIG-PROC-B':
                                [
                                    [
                                        '0',
                                        '.signalprocessors.sp02.attenuation',
                                        'attenuation',
                                        '-signalprocessors-sp02-attenuation',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'NOISE':
                                [
                                    [
                                        '0',
                                        '.noise.mode',
                                        'Mode',
                                        'rx5b-noise-mode',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.noise.debug',
                                        'Debug',
                                        'rx5b-noise-debug',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                },
        },
    'band5b-inaf':
        {
            'INAF Band-5b':  # Clipboard_0
                {
                    'c1':  # col 1
                        {
                            '...':
                                [
                                    [
                                        '0',
                                        '.expected-online',
                                        'expected-online',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.display-errors',
                                        'display-errors',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.startup-state',
                                        'startup-state',
                                        'rx5b-startup-state',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.rsm.opmode',
                                        'rx5b-command',
                                        'rx5b-command',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'rsm':
                                [
                                    [
                                        '0',
                                        '.rsm.uptime-counter',
                                        'uptime-counter',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'c2':
                        {
                            'cryocooler':
                                [
                                    [
                                        '0',
                                        '.vac.valve',
                                        'vac.valve',
                                        'rx5b-vac-valve',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.cryocooler.v24',
                                        'cryocooler.v24',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.cryocooler.vvalve',
                                        'cryocooler.vvalve',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.cryocooler.ivalve',
                                        'cryocooler.ivalve',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.cryocooler.motorspeedmeasure',
                                        'motorspeedmeasure',
                                        'rx5b-cryocooler-motorstartspeed',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '',
                                        '',
                                        'rx5b-cryocooler-motorreinit',
                                        'motorreinit',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.cryocooler.v30',
                                        'v30',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'tempvac':
                                [
                                    [
                                        '0',
                                        '.tempvac.vacuumdewar',
                                        'vacuumdewar',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.tempvac.vacuumsystem',
                                        'vacuumsystem',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'tempvac.temp':
                                [
                                    [
                                        '0',
                                        '.tempvac.temp.stage1',
                                        'stage1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.tempvac.temp.pfs',
                                        'pfs',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.tempvac.temp.stage2',
                                        'stage2',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.tempvac.temp.rfs',
                                        'rfs',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'boxweather.hum01':
                                [
                                    [
                                        '0',
                                        '.boxweather.hum01.humidity',
                                        'humidity',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.boxweather.hum01.temperature',
                                        'temperature',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.boxweather.hum01.pressure',
                                        'pressure',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'boxweather.hum02':
                                [
                                    [
                                        '0',
                                        '.boxweather.hum02.humidity',
                                        'humidity',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.boxweather.hum02.temperature',
                                        'temperature',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.boxweather.hum02.pressure',
                                        'pressure',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'c3':
                        {
                            'atc':
                                [
                                    [
                                        '0',
                                        '.atc.state',
                                        'state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.atc.setpoint',
                                        'setpoint',
                                        'rx5b-atc-setpoint',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.atc.objecttemp',
                                        'objecttemp',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.atc.dutydebug',
                                        'dutydebug',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'psu.volt':
                                [
                                    [
                                        '0',
                                        '.psu.volt.p3v3rcm',
                                        'p3v3rcm',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.volt.p3v3ana',
                                        'p3v3ana',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.volt.p3v3dig',
                                        'p3v3dig',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.volt.p6',
                                        'p6',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.volt.p6dew',
                                        'p6dew',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.volt.n7',
                                        'n7',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.volt.p24',
                                        'p24',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.volt.p30',
                                        'p30',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'psu.cur':
                                [
                                    [
                                        '0',
                                        '.psu.cur.p3v3rcm',
                                        'p3v3rcm',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.cur.p3v3ana',
                                        'p3v3ana',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.cur.p3v3dig',
                                        'p3v3dig',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.cur.p6',
                                        'p6',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.cur.p6dew',
                                        'p6dew',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.cur.n7',
                                        'n7',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.cur.p24',
                                        'p24',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.psu.cur.p30',
                                        'p30',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ]
                        },
                    'c4':
                        {
                            'mmic':
                                [
                                    [
                                        '0',
                                        '.mmic.enable.mmic1',
                                        'enable.mmic1',
                                        'rx5b-mmic-enable-mmic1',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'mmic.drain01':
                                [
                                    [
                                        '0',
                                        '.mmic.drain01.drainvoltage',
                                        'drainvoltage',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.mmic.drain01.drainvoltagemeasure',
                                        'drainvoltagemeasure',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.mmic.drain01.draincurrentmeasure',
                                        'draincurrentmeasure',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'mmic.gate01':
                                [
                                    [
                                        '0',
                                        '.mmic.gate01.gatevoltage',
                                        'gatevoltage',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.mmic.gate01.gatevoltagemeasure',
                                        'gatevoltagemeasure',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'power':
                                [
                                    [
                                        '0',
                                        '.power.rfs',
                                        'rfs',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'signalprocessors.sp01':
                                [
                                    [
                                        '0',
                                        '.signalprocessors.sp01.attenuation',
                                        'attenuation',
                                        'rx5b-signalprocessors-sp01-attenuation',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'noise':
                                [
                                    [
                                        '0',
                                        '.noise.attenuation',
                                        'attenuation',
                                        'rx5b-noise-attenuation',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.noise.monitor',
                                        'monitor',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'c5':
                        {
                            'mmic':
                                [
                                    [
                                        '0',
                                        '.mmic.enable.mmic2',
                                        'enable.mmic2',
                                        'rx5b-mmic-enable-mmic2',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'mmic.drain02':
                                [
                                    [
                                        '0',
                                        '.mmic.drain02.drainvoltage',
                                        'drainvoltage',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.mmic.drain02.drainvoltagemeasure',
                                        'drainvoltagemeasure',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.mmic.drain02.draincurrentmeasure',
                                        'draincurrentmeasure',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'mmic.gate02':
                                [
                                    [
                                        '0',
                                        '.mmic.gate02.gatevoltage',
                                        'gatevoltage',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.mmic.gate02.gatevoltagemeasure',
                                        'gatevoltagemeasure',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            '':  # dummy placeholder
                                [
                                    [
                                        '0',
                                        '',
                                        '',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'signalprocessors.sp02':
                                [
                                    [
                                        '0',
                                        '.signalprocessors.sp02.attenuation',
                                        'attenuation',
                                        '-signalprocessors-sp02-attenuation',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'noise':
                                [
                                    [
                                        '0',
                                        '.noise.mode',
                                        'mode',
                                        'rx5b-noise-mode',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.noise.debug',
                                        'debug',
                                        'rx5b-noise-debug',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                },
        },
    'packetizer':
        {
            'PCK':  # Clipboard_1
                {
                    'r1':  # row 1
                        {
                            'REFERENCE':
                                [
                                    [
                                        '0',
                                        '.packetizer.1pps.length',
                                        'pps-length',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.packetizer.1pps.last',
                                        'pps-last',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.refclk1',
                                        'refclk1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.refclk2',
                                        'refclk2',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.modulus',
                                        'modulus',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'NOISE':
                                [
                                    [
                                        '0',
                                        'digitizer.noiseSource',
                                        'enable',
                                        'debug-noisesource',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'digitizer.noise-diode',
                                        'duty',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'PACK-TEMPS':
                                [
                                    [
                                        '0',
                                        '.packetizer.housekeeping.temperature.qsfp0',
                                        'qsfp0',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.packetizer.housekeeping.temperature.qsfp1',
                                        'qsfp1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.packetizer.housekeeping.temperature.ffly0',
                                        'ffly0',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.packetizer.housekeeping.temperature.ffly1',
                                        'ffly1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.packetizer.housekeeping.temperature.board',
                                        'board',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.packetizer.housekeeping.temperature.fpga',
                                        'board',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.packetizer.housekeeping.temperature.reg1',
                                        'reg1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.packetizer.housekeeping.temperature.reg2',
                                        'reg2',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.packetizer.housekeeping.temperature.cpu0',
                                        'reg2',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'PACK-FAN':
                                [
                                    [
                                        '0',
                                        '.packetizer.fanspeed',
                                        'speed',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.packetizer.housekeeping.revolution.vent_board',
                                        'board',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.packetizer.housekeeping.revolution.vent_frame1',
                                        'frame1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.packetizer.housekeeping.revolution.vent_frame2',
                                        'frame2',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'r2':  # row 1
                        {
                            'DIG-TEMPS':
                                [
                                    [
                                        '0',
                                        '.digitizer.housekeeping.temperature.fpga',
                                        'fpga',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.digitizer.housekeeping.temperature.board',
                                        'board',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.digitizer.housekeeping.temperature.adc0',
                                        'adc0',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.digitizer.housekeeping.temperature.adc1',
                                        'adc1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.digitizer.housekeeping.temperature.ffly0',
                                        'ffly0',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.digitizer.housekeeping.temperature.ffly1',
                                        'ffly1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.digitizer.housekeeping.temperature.4644',
                                        '4644',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'DIG-VOLT-CUR':
                                [
                                    [
                                        '0',
                                        '.digitizer.housekeeping.current.board',
                                        'I-board',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.digitizer.housekeeping.voltage.vcc',
                                        'V-vcc',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.digitizer.housekeeping.voltage.int',
                                        'V-int',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.digitizer.housekeeping.voltage.aux',
                                        'V-aux',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.digitizer.housekeeping.voltage.3V3_synth',
                                        '3v3-synth',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.digitizer.housekeeping.voltage.2V5_synth',
                                        '2V5-synth',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.digitizer.housekeeping.voltage.1V2_adcd',
                                        '1v2-adc',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.digitizer.housekeeping.voltage.3V3',
                                        '3v3',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.digitizer.housekeeping.voltage.3V3_ffly',
                                        '3v3-ffly',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.digitizer.housekeeping.voltage.1V9_adc',
                                        '1v9-adc',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '.digitizer.housekeeping.voltage.1V2_adc_a',
                                        '1v2-adc-a',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                },
        },
    # 'rsc_pck':
    #     {
    #         'RSC':
    #             {
    #                 'r1':
    #                     {
    #                         'STATE':
    #                             [
    #                                 [
    #                                     '0',
    #                                     'rxs.expected-online',
    #                                     'expected-online',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.device-status',
    #                                     'device-status',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.state',
    #                                     'state',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.display-errors',
    #                                     'errors',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.startup-state',
    #                                     'startup-state',
    #                                     'rxs-startup-state',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.rsm.opmode',
    #                                     'operation mode',
    #                                     'rxs-rsm-opmode',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.librxs.systemstate',
    #                                     'libRxS state',
    #                                     'rxs-librxs-switchstate',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.librxs.startupstate',
    #                                     'libRxS startup-state',
    #                                     'rxs-librxs-startupstate',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                             ],
    #                         'DIGITIZER':
    #                             [
    #                                 [
    #                                     '0',
    #                                     'rxs.power.digitizer',
    #                                     'power',
    #                                     'rxs-power-digitizer',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                             ],
    #                         'REFERENCE':
    #                             [
    #                                 [
    #                                     '0',
    #                                     'rxs.tfr.level1pps',
    #                                     '1pps',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.tfr.level100mhz',
    #                                     '100m',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                             ],
    #                         'DOWNCONV':
    #                             [
    #                                 [
    #                                     '0',
    #                                     'rxs.dwnc.frequency',
    #                                     'freq',
    #                                     'rxs-dwnc-frequency',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.dwnc.lock',
    #                                     'lock',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                             ],
    #                         'SYSTEM':
    #                             [
    #                                 [
    #                                     '0',
    #                                     '',
    #                                     '',
    #                                     'rxs-system-eepromall',
    #                                     'write-eeprom',
    #                                     'cell'
    #                                 ],
    #                             ],
    #                     },
    #                 'r2':
    #                     {
    #                         'VALVE':
    #                             [
    #                                 [
    #                                     '0',
    #                                     'rxs.vac-valve-open',
    #                                     'valve',
    #                                     'rxs-cryocooler-vacuumvalve-openclose',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.cryocooler.v24',
    #                                     'V24',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.cryocooler.vvalve',
    #                                     'V-Valve',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.cryocooler.ivalve',
    #                                     'I-Valve',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                             ],
    #                         'CRYO':
    #                             [
    #                                 [
    #                                     '0',
    #                                     'rxs.cryocooler.motorspeedmeasure',
    #                                     'motor',
    #                                     'rxs-cryocooler-motorstartspeed',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     '',
    #                                     '',
    #                                     'rxs-cryocooler-motorreinit',
    #                                     'motor-stop',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.cryocooler.v30',
    #                                     'V30',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                             ],
    #                         'DEWAR':
    #                             [
    #                                 [
    #                                     '0',
    #                                     'rxs.tempvac.temp15k',
    #                                     'T15',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.tempvac.temp70k',
    #                                     'T70',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.tempvac.vacuumdewar',
    #                                     'P-dewar',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.tempvac.vacuumsystem',
    #                                     'P-system',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.dewar.heater20k',
    #                                     'Heater20K',
    #                                     'rxs-dewar-heater20k',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.dewar.heater70k',
    #                                     'Heater70K',
    #                                     'rxs-dewar-heater70k',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                             ],
    #                         'BOX-WEATHER':
    #                             [
    #                                 [
    #                                     '0',
    #                                     'rxs.boxweather.tempcold',
    #                                     'T-cold',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.boxweather.tempwarm',
    #                                     'T-warm',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.boxweather.hum01.humidity',
    #                                     'Hum01',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.boxweather.hum02.humidity',
    #                                     'Hum02',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                             ],
    #                         'ATC':
    #                             [
    #                                 [
    #                                     '0',
    #                                     'rxs.atc.activate',
    #                                     'enabled',
    #                                     'rxs-atc-activate',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.atc.tempisstable',
    #                                     'stable',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.atc.setpoint',
    #                                     'setpoint',
    #                                     'rxs-atc-setpoint',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.atc.objecttemp',
    #                                     'T-obj',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.atc.sinktemp',
    #                                     'T-sink',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.atc.outputcurrent',
    #                                     'I-out',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.atc.outputvoltage',
    #                                     'V-out',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.atc.supplyvoltage',
    #                                     'V-sup',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.atc.device-status',
    #                                     'device-status',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.atc.errornumber',
    #                                     'error',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                             ],
    #                     },
    #                 'r3':
    #                     {
    #                         'CHAIN-A':
    #                             [
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.enable.mmic1',
    #                                     'enabled',
    #                                     'rxs-mmic-enable-mmic1',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                             ],
    #                         'SIG-PROC':
    #                             [
    #                                 [
    #                                     '0',
    #                                     'rxs.signalprocessors.sp01.attenuation',
    #                                     'attenuation',
    #                                     'rxs-signalprocessors-sp01-attenuation',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.signalprocessors.sp01.totalpower',
    #                                     'total-power',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                             ],
    #                         'LNA-1':
    #                             [
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.drain01.drainvoltage',
    #                                     'Vd',
    #                                     'rxs-mmic-drain01-drainvoltage',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.drain01.drainvoltagemeasure',
    #                                     'Vd_mes',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.drain01.draincurrentmeasure',
    #                                     'Id_mes',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.gate01.gatevoltage',
    #                                     'Vg',
    #                                     'rxs-mmic-gate01-gatevoltage',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.gate01.gatevoltagemeasure',
    #                                     'Vg_mes',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                             ],
    #                         'LNA-2':
    #                             [
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.drain02.drainvoltage',
    #                                     'Vd',
    #                                     'rxs-mmic-drain02-drainvoltage',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.drain02.drainvoltagemeasure',
    #                                     'Vd_mes',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.drain02.draincurrentmeasure',
    #                                     'Id_mes',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.gate02.gatevoltage',
    #                                     'Vg',
    #                                     'rxs-mmic-gate02-gatevoltage',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.gate02.gatevoltagemeasure',
    #                                     'Vg_mes',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                             ],
    #                         'LNA-3':
    #                             [
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.drain03.drainvoltage',
    #                                     'Vd',
    #                                     'rxs-mmic-drain03-drainvoltage',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.drain03.drainvoltagemeasure',
    #                                     'Vd_mes',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.drain03.draincurrentmeasure',
    #                                     'Id_mes',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.gate03.gatevoltage',
    #                                     'Vg',
    #                                     'rxs-mmic-gate03-gatevoltage',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.gate03.gatevoltagemeasure',
    #                                     'Vg_mes',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                             ],
    #                     },
    #                 'r4':
    #                     {
    #                         'CHAIN-B':
    #                             [
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.enable.mmic2',
    #                                     'enabled',
    #                                     'rxs-mmic-enable-mmic2',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                             ],
    #                         'SIG-PROC':
    #                             [
    #                                 [
    #                                     '0',
    #                                     'rxs.signalprocessors.sp02.attenuation',
    #                                     'attenuation',
    #                                     'rxs-signalprocessors-sp02-attenuation',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.signalprocessors.sp02.totalpower',
    #                                     'total-power',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                             ],
    #                         'LNA-1':
    #                             [
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.drain04.drainvoltage',
    #                                     'Vd',
    #                                     'rxs-mmic-drain04-drainvoltage',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.drain04.drainvoltagemeasure',
    #                                     'Vd_mes',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.drain04.draincurrentmeasure',
    #                                     'Id_mes',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.gate04.gatevoltage',
    #                                     'Vg',
    #                                     'rxs-mmic-gate04-gatevoltage',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.gate04.gatevoltagemeasure',
    #                                     'Vg_mes',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                             ],
    #                         'LNA-2':
    #                             [
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.drain05.drainvoltage',
    #                                     'Vd',
    #                                     'rxs-mmic-drain05-drainvoltage',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.drain05.drainvoltagemeasure',
    #                                     'Vd_mes',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.drain05.draincurrentmeasure',
    #                                     'Id_mes',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.gate05.gatevoltage',
    #                                     'Vg',
    #                                     'rxs-mmic-gate05-gatevoltage',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.gate05.gatevoltagemeasure',
    #                                     'Vg_mes',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                             ],
    #                         'LNA-3':
    #                             [
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.drain06.drainvoltage',
    #                                     'Vd',
    #                                     'rxs-mmic-drain06-drainvoltage',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.drain06.drainvoltagemeasure',
    #                                     'Vd_mes',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.drain06.draincurrentmeasure',
    #                                     'Id_mes',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.gate06.gatevoltage',
    #                                     'Vg',
    #                                     'rxs-mmic-gate06-gatevoltage',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '0',
    #                                     'rxs.mmic.gate06.gatevoltagemeasure',
    #                                     'Vg_mes',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                             ],
    #                     },
    #             },
    #         'PCK':  # Clipboard_1
    #             {
    #                 'r1':  # row 1
    #                     {
    #                         'REFERENCE':
    #                             [
    #                                 [
    #                                     '1',
    #                                     'rxs.packetizer.1pps.length',
    #                                     'pps-length',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.packetizer.1pps.last',
    #                                     'pps-last',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.packetizer.freq.refclk1',
    #                                     'refclk1',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.packetizer.freq.refclk2',
    #                                     'refclk2',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     's-band.scg.time.stamps.modulus',
    #                                     'modulus',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                             ],
    #                         'NOISE':
    #                             [
    #                                 [
    #                                     '1',
    #                                     'rxs.digitizer.noiseSource',
    #                                     'enable',
    #                                     'rxs-debug-noisesource',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     's-band.noise-diode',
    #                                     'duty',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                             ],
    #                         'PACK-TEMPS':
    #                             [
    #                                 [
    #                                     '1',
    #                                     'rxs.packetizer.housekeeping.temperature.qsfp0',
    #                                     'qsfp0',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.packetizer.housekeeping.temperature.qsfp1',
    #                                     'qsfp1',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.packetizer.housekeeping.temperature.ffly0',
    #                                     'ffly0',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.packetizer.housekeeping.temperature.ffly1',
    #                                     'ffly1',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.packetizer.housekeeping.temperature.board',
    #                                     'board',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.packetizer.housekeeping.temperature.fpga',
    #                                     'board',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.packetizer.housekeeping.temperature.reg1',
    #                                     'reg1',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.packetizer.housekeeping.temperature.reg2',
    #                                     'reg2',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.packetizer.housekeeping.temperature.cpu0',
    #                                     'reg2',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                             ],
    #                         'PACK-FAN':
    #                             [
    #                                 [
    #                                     '1',
    #                                     'rxs.packetizer.fanspeed',
    #                                     'speed',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.packetizer.housekeeping.revolution.vent_board',
    #                                     'board',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.packetizer.housekeeping.revolution.vent_frame1',
    #                                     'frame1',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.packetizer.housekeeping.revolution.vent_frame2',
    #                                     'frame2',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                             ],
    #                     },
    #                 'r2':  # row 1
    #                     {
    #                         'DIG-TEMPS':
    #                             [
    #                                 [
    #                                     '1',
    #                                     'rxs.digitizer.housekeeping.temperature.fpga',
    #                                     'fpga',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.digitizer.housekeeping.temperature.board',
    #                                     'board',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.digitizer.housekeeping.temperature.adc0',
    #                                     'adc0',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.digitizer.housekeeping.temperature.adc1',
    #                                     'adc1',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.digitizer.housekeeping.temperature.ffly0',
    #                                     'ffly0',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.digitizer.housekeeping.temperature.ffly1',
    #                                     'ffly1',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.digitizer.housekeeping.temperature.4644',
    #                                     '4644',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                             ],
    #                         'DIG-VOLT-CUR':
    #                             [
    #                                 [
    #                                     '1',
    #                                     'rxs.digitizer.housekeeping.current.board',
    #                                     'I-board',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.digitizer.housekeeping.voltage.vcc',
    #                                     'V-vcc',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.digitizer.housekeeping.voltage.int',
    #                                     'V-int',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.digitizer.housekeeping.voltage.aux',
    #                                     'V-aux',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.digitizer.housekeeping.voltage.3V3_synth',
    #                                     '3v3-synth',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.digitizer.housekeeping.voltage.2V5_synth',
    #                                     '2V5-synth',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.digitizer.housekeeping.voltage.1V2_adcd',
    #                                     '1v2-adc',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.digitizer.housekeeping.voltage.3V3',
    #                                     '3v3',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.digitizer.housekeeping.voltage.3V3_ffly',
    #                                     '3v3-ffly',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.digitizer.housekeeping.voltage.1V9_adc',
    #                                     '1v9-adc',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                                 [
    #                                     '1',
    #                                     'rxs.digitizer.housekeeping.voltage.1V2_adc_a',
    #                                     '1v2-adc-a',
    #                                     '',
    #                                     '',
    #                                     'cell'
    #                                 ],
    #                             ],
    #                     },
    #             },
    #     },
    'rsc_pck_v2':
        {
            'RSC':
                {
                    'c1':  # col 1
                        {
                            'STATES':
                                [
                                    [
                                        '0',
                                        'rxs.expected-online',
                                        'expected-online',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.state',
                                        'state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.display-errors',
                                        'errors',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.startup-state',
                                        'startup-state',
                                        'rxs-startup-state',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.rsm.opmode',
                                        'operation mode',
                                        'rxs-rsm-opmode',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'STATEMACHINE':
                                [
                                    [
                                        '0',
                                        'rxs.rsm.statemachinestate',
                                        'sm-state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.rsm.tout-interval',
                                        'tout-interval',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.rsm.tout-counter',
                                        'tout-counter',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.rsm.uptime-counter',
                                        'uptime',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LIBRXS':
                                [
                                    [
                                        '0',
                                        'rxs.librxs.systemstate',
                                        'libRxS state',
                                        'rxs-librxs-switchstate',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.librxs.startupstate',
                                        'libRxS startup-state',
                                        'rxs-librxs-startupstate',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'DIGITIZER':
                                [
                                    [
                                        '0',
                                        'rxs.power.digitizer',
                                        'power',
                                        'rxs-power-digitizer',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'REFERENCE':
                                [
                                    [
                                        '0',
                                        'rxs.tfr.level1pps',
                                        '1pps',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.tfr.level100mhz',
                                        '100m',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'DOWNCONV':
                                [
                                    [
                                        '0',
                                        'rxs.dwnc.frequency',
                                        'freq',
                                        'rxs-dwnc-frequency',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.dwnc.lock',
                                        'lock',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'SYSTEM':
                                [
                                    [
                                        '0',
                                        '',
                                        '',
                                        'rxs-system-eepromall',
                                        'write-eeprom',
                                        'cell'
                                    ],
                                ],
                            'TANGO':
                                [
                                    [
                                        '0',
                                        'rxs.translator.rsm.vacrequest',
                                        'vac-req',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.translator.rsm.herequest',
                                        'he-req',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'c2':
                        {
                            'VALVE':
                                [
                                    [
                                        '0',
                                        'rxs.vac-valve-open',
                                        'valve',
                                        'rxs-cryocooler-vacuumvalve-openclose',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.cryocooler.v24',
                                        'V24',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.cryocooler.vvalve',
                                        'V-Valve',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.cryocooler.ivalve',
                                        'I-Valve',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'CRYO':
                                [
                                    [
                                        '0',
                                        'rxs.cryocooler.motorspeedmeasure',
                                        'motor',
                                        'rxs-cryocooler-motorstartspeed',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '',
                                        '',
                                        'rxs-cryocooler-motorreinit',
                                        'motor-stop',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.cryocooler.v30',
                                        'V30',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'DEWAR':
                                [
                                    [
                                        '0',
                                        'rxs.tempvac.temp15k',
                                        'T15',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.tempvac.temp70k',
                                        'T70',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.tempvac.vacuumdewar',
                                        'P-dewar',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.tempvac.vacuumsystem',
                                        'P-system',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.dewar.heater20k',
                                        'Heater20K',
                                        'rxs-dewar-heater20k',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.dewar.heater70k',
                                        'Heater70K',
                                        'rxs-dewar-heater70k',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'BOX-WEATHER':
                                [
                                    [
                                        '0',
                                        'rxs.boxweather.tempcold',
                                        'T-cold',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.boxweather.tempwarm',
                                        'T-warm',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.boxweather.hum01.humidity',
                                        'Hum01',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.boxweather.hum02.humidity',
                                        'Hum02',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'ATC':
                                [
                                    [
                                        '0',
                                        'rxs.atc.activate',
                                        'enabled',
                                        'rxs-atc-activate',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.tempisstable',
                                        'stable',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.setpoint',
                                        'setpoint',
                                        'rxs-atc-setpoint',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.objecttemp',
                                        'T-obj',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.sinktemp',
                                        'T-sink',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.outputcurrent',
                                        'I-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.outputvoltage',
                                        'V-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.supplyvoltage',
                                        'V-sup',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.errornumber',
                                        'error',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'c3':
                        {
                            'CHAIN-A':
                                [
                                    [
                                        '0',
                                        'rxs.mmic.enable.mmic1',
                                        'enabled',
                                        'rxs-mmic-enable-mmic1',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'SIG-PROC':
                                [
                                    [
                                        '0',
                                        'rxs.signalprocessors.sp01.attenuation',
                                        'attenuation',
                                        'rxs-signalprocessors-sp01-attenuation',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.signalprocessors.sp01.totalpower',
                                        'total-power',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-1':
                                [
                                    [
                                        '0',
                                        'rxs.mmic.drain01.drainvoltage',
                                        'Vd',
                                        'rxs-mmic-drain01-drainvoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.drain01.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.drain01.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.gate01.gatevoltage',
                                        'Vg',
                                        'rxs-mmic-gate01-gatevoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.gate01.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-2':
                                [
                                    [
                                        '0',
                                        'rxs.mmic.drain02.drainvoltage',
                                        'Vd',
                                        'rxs-mmic-drain02-drainvoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.drain02.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.drain02.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.gate02.gatevoltage',
                                        'Vg',
                                        'rxs-mmic-gate02-gatevoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.gate02.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-3':
                                [
                                    [
                                        '0',
                                        'rxs.mmic.drain03.drainvoltage',
                                        'Vd',
                                        'rxs-mmic-drain03-drainvoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.drain03.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.drain03.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.gate03.gatevoltage',
                                        'Vg',
                                        'rxs-mmic-gate03-gatevoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.gate03.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'c4':
                        {
                            'CHAIN-B':
                                [
                                    [
                                        '0',
                                        'rxs.mmic.enable.mmic2',
                                        'enabled',
                                        'rxs-mmic-enable-mmic2',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'SIG-PROC':
                                [
                                    [
                                        '0',
                                        'rxs.signalprocessors.sp02.attenuation',
                                        'attenuation',
                                        'rxs-signalprocessors-sp02-attenuation',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.signalprocessors.sp02.totalpower',
                                        'total-power',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-1':
                                [
                                    [
                                        '0',
                                        'rxs.mmic.drain04.drainvoltage',
                                        'Vd',
                                        'rxs-mmic-drain04-drainvoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.drain04.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.drain04.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.gate04.gatevoltage',
                                        'Vg',
                                        'rxs-mmic-gate04-gatevoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.gate04.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-2':
                                [
                                    [
                                        '0',
                                        'rxs.mmic.drain05.drainvoltage',
                                        'Vd',
                                        'rxs-mmic-drain05-drainvoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.drain05.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.drain05.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.gate05.gatevoltage',
                                        'Vg',
                                        'rxs-mmic-gate05-gatevoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.gate05.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-3':
                                [
                                    [
                                        '0',
                                        'rxs.mmic.drain06.drainvoltage',
                                        'Vd',
                                        'rxs-mmic-drain06-drainvoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.drain06.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.drain06.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.gate06.gatevoltage',
                                        'Vg',
                                        'rxs-mmic-gate06-gatevoltage',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.mmic.gate06.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                },
            'PCK':  # Clipboard_1
                {
                    'r1':  # row 1
                        {
                            'REFERENCE':
                                [
                                    [
                                        '1',
                                        '.packetizer.1pps.length',
                                        'pps-length',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.packetizer.1pps.last',
                                        'pps-last',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.refclk1',
                                        'refclk1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.refclk2',
                                        'refclk2',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.modulus',
                                        'modulus',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'NOISE':
                                [
                                    [
                                        '1',
                                        'digitizer.noiseSource',
                                        'enable',
                                        'debug-noisesource',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        'digitizer.noise-diode',
                                        'duty',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'PACK-TEMPS':
                                [
                                    [
                                        '1',
                                        '.packetizer.housekeeping.temperature.qsfp0',
                                        'qsfp0',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.packetizer.housekeeping.temperature.qsfp1',
                                        'qsfp1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.packetizer.housekeeping.temperature.ffly0',
                                        'ffly0',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.packetizer.housekeeping.temperature.ffly1',
                                        'ffly1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.packetizer.housekeeping.temperature.board',
                                        'board',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.packetizer.housekeeping.temperature.fpga',
                                        'board',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.packetizer.housekeeping.temperature.reg1',
                                        'reg1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.packetizer.housekeeping.temperature.reg2',
                                        'reg2',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.packetizer.housekeeping.temperature.cpu0',
                                        'reg2',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'PACK-FAN':
                                [
                                    [
                                        '1',
                                        '.packetizer.fanspeed',
                                        'speed',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.packetizer.housekeeping.revolution.vent_board',
                                        'board',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.packetizer.housekeeping.revolution.vent_frame1',
                                        'frame1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.packetizer.housekeeping.revolution.vent_frame2',
                                        'frame2',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'r2':  # row 1
                        {
                            'DIG-TEMPS':
                                [
                                    [
                                        '1',
                                        '.digitizer.housekeeping.temperature.fpga',
                                        'fpga',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.digitizer.housekeeping.temperature.board',
                                        'board',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.digitizer.housekeeping.temperature.adc0',
                                        'adc0',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.digitizer.housekeeping.temperature.adc1',
                                        'adc1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.digitizer.housekeeping.temperature.ffly0',
                                        'ffly0',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.digitizer.housekeeping.temperature.ffly1',
                                        'ffly1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.digitizer.housekeeping.temperature.4644',
                                        '4644',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'DIG-VOLT-CUR':
                                [
                                    [
                                        '1',
                                        '.digitizer.housekeeping.current.board',
                                        'I-board',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.digitizer.housekeeping.voltage.vcc',
                                        'V-vcc',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.digitizer.housekeeping.voltage.int',
                                        'V-int',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.digitizer.housekeeping.voltage.aux',
                                        'V-aux',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.digitizer.housekeeping.voltage.3V3_synth',
                                        '3v3-synth',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.digitizer.housekeeping.voltage.2V5_synth',
                                        '2V5-synth',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.digitizer.housekeeping.voltage.1V2_adcd',
                                        '1v2-adc',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.digitizer.housekeeping.voltage.3V3',
                                        '3v3',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.digitizer.housekeeping.voltage.3V3_ffly',
                                        '3v3-ffly',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.digitizer.housekeeping.voltage.1V9_adc',
                                        '1v9-adc',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        '.digitizer.housekeeping.voltage.1V2_adc_a',
                                        '1v2-adc-a',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                },
        },
    'edd_rcm':
        {
            'RCM':  # Clipboard_0
                {
                    'c1':  # col 1
                        {
                            'STATE':
                                [
                                    [
                                        '0',
                                        'rxs.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.state',
                                        'state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.rsm.statemachinestate',
                                        'statemachine',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.display-errors',
                                        'errors',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.startupstate',
                                        'startup-state',
                                        'rxs-startup-state',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.rsm.opmode',
                                        'operation mode',
                                        'rxs-rsm-opmode',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'POWER':
                                [
                                    [
                                        '0',
                                        'rxs.digitizer01.enable',
                                        'digitizer01',
                                        'rxs-digitizer01-enable',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.digitizer02.enable',
                                        'digitizer02',
                                        'rxs-digitizer02-enable',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.tfr.enable',
                                        'tfr',
                                        'rxs-tfr-enable',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'REFERENCE':
                                [
                                    [
                                        '0',
                                        'rxs.tfr.level1pps',
                                        '1pps',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.tfr.level100mhz',
                                        '100m',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LIFT':
                                [
                                    [
                                        '0',
                                        '',
                                        'stop',
                                        'rxs-motorlift-stop',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '',
                                        'move-in',
                                        'rxs-motorlift-movein',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '',
                                        'move-out',
                                        'rxs-motorlift-moveout',
                                        '',
                                        'cell'
                                    ],
                                ]
                        },
                    'c2':
                        {
                            'CRYO':
                                [
                                    [
                                        '0',
                                        '',  # 'rxs.cryocooler.motorspeedmeasure',
                                        'motor-start',
                                        'rxs-cryocooler-motorstart',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '',
                                        '',
                                        'rxs-cryocooler-motorstop',
                                        'motor-stop',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.cryocooler.v30',
                                        'V30',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'DEWAR':
                                [
                                    [
                                        '0',
                                        'rxs.tempvac.temp15k',
                                        'T15',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.tempvac.temp70k',
                                        'T70',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.tempvac.vacuumdewar',
                                        'P-dewar',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.dewar.heater20k',
                                        'Heater20K',
                                        'rxs-dewar-heater20k',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.dewar.heater70k',
                                        'Heater70K',
                                        'rxs-dewar-heater70k',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'BOX-WEATHER':
                                [
                                    [
                                        '0',
                                        'rxs.boxweather.tempcold',
                                        'T-cold',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.boxweather.tempwarm',
                                        'T-warm',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.boxweather.hum01.humidity',
                                        'Hum01',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.boxweather.hum02.humidity',
                                        'Hum02',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'ATC-A':
                                [
                                    [
                                        '0',
                                        'rxs.atc.atc1.activate',
                                        'enabled',
                                        'rxs-atc-atc1-activate',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.atc1.tempisstable',
                                        'stable',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.atc1.setpoint',
                                        'setpoint',
                                        'rxs-atc-atc1-setpoint',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.atc1.objecttemp',
                                        'T-obj',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.atc1.sinktemp',
                                        'T-sink',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.atc1.outputcurrent',
                                        'I-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.atc1.outputvoltage',
                                        'V-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.atc1.supplyvoltage',
                                        'V-sup',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.atc1.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.atc1.errornumber',
                                        'error',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'ATC-B':
                                [
                                    [
                                        '0',
                                        'rxs.atc.atc2.activate',
                                        'enabled',
                                        'rxs-atc-atc2-activate',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.atc2.tempisstable',
                                        'stable',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.atc2.setpoint',
                                        'setpoint',
                                        'rxs-atc-atc2-setpoint',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.atc2.objecttemp',
                                        'T-obj',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.atc2.sinktemp',
                                        'T-sink',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.atc2.outputcurrent',
                                        'I-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.atc2.outputvoltage',
                                        'V-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.atc2.supplyvoltage',
                                        'V-sup',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.atc2.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.atc.atc2.errornumber',
                                        'error',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'c3':
                        {
                            'CHAIN-A1':
                                [
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.enable01',
                                        'enabled',
                                        'rxs-horn0-mmic0-enable01',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'SIG-PROC':
                                [
                                    [
                                        '0',
                                        'rxs.signalprocessors.spa1.attenuation',
                                        'attenuation',
                                        'rxs-signalprocessors-spa1-attenuation',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.signalprocessors.spa1.totalpower',
                                        'total-power',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-1':
                                [
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.drain01.drainvoltageset',
                                        'Vd',
                                        'rxs-horn0-mmic0-drain01-drainvoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.drain01.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.drain01.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.gate01.gatevoltageset',
                                        'Vg',
                                        'rxs-horn0-mmic0-gate01-gatevoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.gate01.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-2':
                                [
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.drain02.drainvoltageset',
                                        'Vd',
                                        'rxs-horn0-mmic0-drain02-drainvoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.drain02.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.drain02.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.gate02.gatevoltageset',
                                        'Vg',
                                        'rxs-horn0-mmic0-gate02-gatevoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.gate02.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-3':
                                [
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.drain03.drainvoltageset',
                                        'Vd',
                                        'rxs-horn0-mmic0-drain03-drainvoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.drain03.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.drain03.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.gate03.gatevoltageset',
                                        'Vg',
                                        'rxs-horn0-mmic0-gate03-gatevoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.gate03.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'c4':
                        {
                            'CHAIN-A2':
                                [
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.enable02',
                                        'enabled',
                                        'rxs-horn0-mmic0-enable02',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'SIG-PROC':
                                [
                                    [
                                        '0',
                                        'rxs.signalprocessors.spa2.attenuation',
                                        'attenuation',
                                        'rxs-signalprocessors-spa2-attenuation',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.signalprocessors.spa2.totalpower',
                                        'total-power',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-1':
                                [
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.drain05.drainvoltageset',
                                        'Vd',
                                        'rxs-horn0-mmic0-drain05-drainvoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.drain05.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.drain05.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.gate05.gatevoltageset',
                                        'Vg',
                                        'rxs-horn0-mmic0-gate05-gatevoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.gate05.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-2':
                                [
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.drain06.drainvoltageset',
                                        'Vd',
                                        'rxs-horn0-mmic0-drain06-drainvoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.drain06.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.drain06.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.gate06.gatevoltageset',
                                        'Vg',
                                        'rxs-horn0-mmic0-gate06-gatevoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.gate06.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-3':
                                [
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.drain07.drainvoltageset',
                                        'Vd',
                                        'rxs-horn0-mmic0-drain07-drainvoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.drain07.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.drain07.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.gate07.gatevoltageset',
                                        'Vg',
                                        'rxs-horn0-mmic0-gate07-gatevoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic0.gate07.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'c5':
                        {
                            'CHAIN-B1':
                                [
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.enable01',
                                        'enabled',
                                        'rxs-horn0-mmic1-enable01',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'SIG-PROC':
                                [
                                    [
                                        '0',
                                        'rxs.signalprocessors.spb1.attenuation',
                                        'attenuation',
                                        'rxs-signalprocessors-spb1-attenuation',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.signalprocessors.spb1.totalpower',
                                        'total-power',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-1':
                                [
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.drain01.drainvoltageset',
                                        'Vd',
                                        'rxs-horn0-mmic1-drain01-drainvoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.drain01.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.drain01.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.gate01.gatevoltageset',
                                        'Vg',
                                        'rxs-horn0-mmic1-gate01-gatevoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.gate01.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-2':
                                [
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.drain02.drainvoltageset',
                                        'Vd',
                                        'rxs-horn0-mmic1-drain02-drainvoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.drain02.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.drain02.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.gate02.gatevoltageset',
                                        'Vg',
                                        'rxs-horn0-mmic1-gate02-gatevoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.gate02.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-3':
                                [
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.drain03.drainvoltageset',
                                        'Vd',
                                        'rxs-horn0-mmic1-drain03-drainvoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.drain03.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.drain03.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.gate03.gatevoltageset',
                                        'Vg',
                                        'rxs-horn0-mmic1-gate03-gatevoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.gate03.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'c6':
                        {
                            'CHAIN-B2':
                                [
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.enable02',
                                        'enabled',
                                        'rxs-horn0-mmic1-enable02',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'SIG-PROC':
                                [
                                    [
                                        '0',
                                        'rxs.signalprocessors.spb2.attenuation',
                                        'attenuation',
                                        'rxs-signalprocessors-spb2-attenuation',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.signalprocessors.spb2.totalpower',
                                        'total-power',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-1':
                                [
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.drain05.drainvoltageset',
                                        'Vd',
                                        'rxs-horn0-mmic1-drain05-drainvoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.drain05.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.drain05.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.gate05.gatevoltageset',
                                        'Vg',
                                        'rxs-horn0-mmic1-gate05-gatevoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.gate05.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-2':
                                [
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.drain06.drainvoltageset',
                                        'Vd',
                                        'rxs-horn0-mmic1-drain06-drainvoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.drain06.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.drain06.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.gate06.gatevoltageset',
                                        'Vg',
                                        'rxs-horn0-mmic1-gate06-gatevoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.gate06.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-3':
                                [
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.drain07.drainvoltageset',
                                        'Vd',
                                        'rxs-horn0-mmic1-drain07-drainvoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.drain07.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.drain07.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.gate07.gatevoltageset',
                                        'Vg',
                                        'rxs-horn0-mmic1-gate07-gatevoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.horn0.mmic1.gate07.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                }
        },
    'edd_rcm_ubb':
        {
            'RCM':  # Clipboard_0
                {
                    'c1':  # col 1
                        {
                            'STATE':
                                [
                                    [
                                        '0',
                                        'rxubb.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.state',
                                        'state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.rsm.statemachinestate',
                                        'statemachine',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.display-errors',
                                        'errors',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.startupstate',
                                        'startup-state',
                                        'rxubb-startup-state',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.rsm.opmode',
                                        'operation mode',
                                        'rxubb-rsm-opmode',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'POWER':
                                [
                                    [
                                        '0',
                                        'rxubb.digitizer01.enable',
                                        'digitizer01',
                                        'rxubb-digitizer01-enable',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.digitizer02.enable',
                                        'digitizer02',
                                        'rxubb-digitizer02-enable',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.tfr.enable',
                                        'tfr',
                                        'rxubb-tfr-enable',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'REFERENCE':
                                [
                                    [
                                        '0',
                                        'rxubb.tfr.level1pps',
                                        '1pps',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.tfr.level100mhz',
                                        '100m',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LIFT':
                                [
                                    [
                                        '0',
                                        '',
                                        'stop',
                                        'rxubb-motorlift-stop',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '',
                                        'move-in',
                                        'rxubb-motorlift-movein',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '',
                                        'move-out',
                                        'rxubb-motorlift-moveout',
                                        '',
                                        'cell'
                                    ],
                                ]
                        },
                    'c2':
                        {
                            'CRYO':
                                [
                                    [
                                        '0',
                                        '',  # 'rxubb.cryocooler.motorspeedmeasure',
                                        'motor-start',
                                        'rxubb-cryocooler-motorstart',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        '',
                                        '',
                                        'rxubb-cryocooler-motorstop',
                                        'motor-stop',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.cryocooler.v30',
                                        'V30',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'DEWAR':
                                [
                                    [
                                        '0',
                                        'rxubb.tempvac.temp15k',
                                        'T15',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.tempvac.temp70k',
                                        'T70',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.tempvac.vacuumdewar',
                                        'P-dewar',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.dewar.heater20k',
                                        'Heater20K',
                                        'rxubb-dewar-heater20k',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.dewar.heater70k',
                                        'Heater70K',
                                        'rxubb-dewar-heater70k',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'BOX-WEATHER':
                                [
                                    [
                                        '0',
                                        'rxubb.boxweather.tempcold',
                                        'T-cold',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.boxweather.tempwarm',
                                        'T-warm',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.boxweather.hum01.humidity',
                                        'Hum01',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.boxweather.hum02.humidity',
                                        'Hum02',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'ATC-1':
                                [
                                    [
                                        '0',
                                        'rxubb.atc.atc1.activate',
                                        'enabled',
                                        'rxubb-atc-atc1-activate',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.atc.atc1.tempisstable',
                                        'stable',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.atc.atc1.setpoint',
                                        'setpoint',
                                        'rxubb-atc-atc1-setpoint',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.atc.atc1.objecttemp',
                                        'T-obj',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.atc.atc1.sinktemp',
                                        'T-sink',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.atc.atc1.outputcurrent',
                                        'I-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.atc.atc1.outputvoltage',
                                        'V-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.atc.atc1.supplyvoltage',
                                        'V-sup',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.atc.atc1.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.atc.atc1.errornumber',
                                        'error',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'ATC-2':
                                [
                                    [
                                        '0',
                                        'rxubb.atc.atc2.activate',
                                        'enabled',
                                        'rxubb-atc-atc2-activate',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.atc.atc2.tempisstable',
                                        'stable',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.atc.atc2.setpoint',
                                        'setpoint',
                                        'rxubb-atc-atc2-setpoint',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.atc.atc2.objecttemp',
                                        'T-obj',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.atc.atc2.sinktemp',
                                        'T-sink',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.atc.atc2.outputcurrent',
                                        'I-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.atc.atc2.outputvoltage',
                                        'V-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.atc.atc2.supplyvoltage',
                                        'V-sup',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.atc.atc2.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.atc.atc2.errornumber',
                                        'error',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'c3':
                        {
                            'HORN_1::CHAIN_1':
                                [
                                    [
                                        '0',
                                        'rxubb.horn0.mmic0.enable01',
                                        'enabled',
                                        'rxubb-horn0-mmic0-enable01',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-1':
                                [
                                    [
                                        '0',
                                        'rxubb.horn0.mmic0.drain01.drainvoltageset',
                                        'Vd',
                                        'rxubb-horn0-mmic0-drain01-drainvoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.horn0.mmic0.drain01.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.horn0.mmic0.drain01.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.horn0.mmic0.gate01.gatevoltageset',
                                        'Vg',
                                        'rxubb-horn0-mmic0-gate01-gatevoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.horn0.mmic0.gate01.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'SP_1::ADC_1':
                                [
                                    [
                                        '0',
                                        'rxubb.signalprocessors.sp1adc1.attenuation',
                                        'attenuation',
                                        'rxubb-signalprocessors-sp1adc1-attenuation',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.signalprocessors.sp1adc1.totalpower',
                                        'total-power',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'SP_2::ADC_1':
                                [
                                    [
                                        '0',
                                        'rxubb.signalprocessors.sp2adc1.attenuation',
                                        'attenuation',
                                        'rxubb-signalprocessors-sp2adc1-attenuation',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.signalprocessors.sp2adc1.totalpower',
                                        'total-power',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'c4':
                        {
                            'HORN_1::CHAIN_2':
                                [
                                    [
                                        '0',
                                        'rxubb.horn0.mmic0.enable02',
                                        'enabled',
                                        'rxubb-horn0-mmic0-enable02',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'LNA-1':
                                [
                                    [
                                        '0',
                                        'rxubb.horn0.mmic0.drain05.drainvoltageset',
                                        'Vd',
                                        'rxubb-horn0-mmic0-drain05-drainvoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.horn0.mmic0.drain05.drainvoltagemeasure',
                                        'Vd_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.horn0.mmic0.drain05.draincurrentmeasure',
                                        'Id_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.horn0.mmic0.gate05.gatevoltageset',
                                        'Vg',
                                        'rxubb-horn0-mmic0-gate05-gatevoltageset',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.horn0.mmic0.gate05.gatevoltagemeasure',
                                        'Vg_mes',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'SP_1::ADC_0':
                                [
                                    [
                                        '0',
                                        'rxubb.signalprocessors.sp1adc0.attenuation',
                                        'attenuation',
                                        'rxubb-signalprocessors-sp1adc0-attenuation',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.signalprocessors.sp1adc0.totalpower',
                                        'total-power',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'SP_2::ADC_0':
                                [
                                    [
                                        '0',
                                        'rxubb.signalprocessors.sp2adc0.attenuation',
                                        'attenuation',
                                        'rxubb-signalprocessors-sp2adc0-attenuation',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxubb.signalprocessors.sp2adc0.totalpower',
                                        'total-power',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    # 'c5':
                    #     {
                    #         'CHAIN-B1 :: SIG-PROC':
                    #             [
                    #                 [
                    #                     '0',
                    #                     'rxubb.signalprocessors.spb1.attenuation',
                    #                     'attenuation',
                    #                     'rxubb-signalprocessors-spb1-attenuation',
                    #                     '',
                    #                     'cell'
                    #                 ],
                    #                 [
                    #                     '0',
                    #                     'rxubb.signalprocessors.spb1.totalpower',
                    #                     'total-power',
                    #                     '',
                    #                     '',
                    #                     'cell'
                    #                 ],
                    #             ],
                    #     },
                    # 'c6':
                    #     {
                    #         'CHAIN-B2 :: SIG-PROC':
                    #             [
                    #                 [
                    #                     '0',
                    #                     'rxubb.signalprocessors.spb2.attenuation',
                    #                     'attenuation',
                    #                     'rxubb-signalprocessors-spb2-attenuation',
                    #                     '',
                    #                     'cell'
                    #                 ],
                    #                 [
                    #                     '0',
                    #                     'rxubb.signalprocessors.spb2.totalpower',
                    #                     'total-power',
                    #                     '',
                    #                     '',
                    #                     'cell'
                    #                 ],
                    #             ],
                    #     },
                }
        },
    'gen_sband_pck':
        {
            'PCK':
                {
                    'r1':  # row 1
                        {
                            'REFERENCE':
                                [
                                    [
                                        '0',
                                        'rxs.packetizer.1pps.length',
                                        'pps-length',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.packetizer.1pps.last',
                                        'pps-last',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.packetizer.freq.refclk1',
                                        'refclk1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.packetizer.freq.refclk2',
                                        'refclk2',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band.scg.time.stamps.modulus',
                                        'modulus',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'NOISE':
                                [
                                    [
                                        '0',
                                        'rxs.digitizer.noiseSource',
                                        'enable',
                                        'rxs-debug-noisesource',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band.noise-diode',
                                        'duty',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'PACK-TEMPS':
                                [
                                    [
                                        '0',
                                        'rxs.packetizer.housekeeping.temperature.qsfp0',
                                        'qsfp0',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.packetizer.housekeeping.temperature.qsfp1',
                                        'qsfp1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.packetizer.housekeeping.temperature.ffly0',
                                        'ffly0',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.packetizer.housekeeping.temperature.ffly1',
                                        'ffly1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.packetizer.housekeeping.temperature.board',
                                        'board',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.packetizer.housekeeping.temperature.fpga',
                                        'board',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.packetizer.housekeeping.temperature.reg1',
                                        'reg1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.packetizer.housekeeping.temperature.reg2',
                                        'reg2',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.packetizer.housekeeping.temperature.cpu0',
                                        'reg2',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'PACK-FAN':
                                [
                                    [
                                        '0',
                                        'rxs.packetizer.fanspeed',
                                        'speed',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.packetizer.housekeeping.revolution.vent_board',
                                        'board',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.packetizer.housekeeping.revolution.vent_frame1',
                                        'frame1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.packetizer.housekeeping.revolution.vent_frame2',
                                        'frame2',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'r2':  # row 1
                        {
                            'DIG-TEMPS':
                                [
                                    [
                                        '0',
                                        'rxs.digitizer.housekeeping.temperature.fpga',
                                        'fpga',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.digitizer.housekeeping.temperature.board',
                                        'board',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.digitizer.housekeeping.temperature.adc0',
                                        'adc0',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.digitizer.housekeeping.temperature.adc1',
                                        'adc1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.digitizer.housekeeping.temperature.ffly0',
                                        'ffly0',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.digitizer.housekeeping.temperature.ffly1',
                                        'ffly1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.digitizer.housekeeping.temperature.4644',
                                        '4644',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'DIG-VOLT-CUR':
                                [
                                    [
                                        '0',
                                        'rxs.digitizer.housekeeping.current.board',
                                        'I-board',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.digitizer.housekeeping.voltage.vcc',
                                        'V-vcc',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.digitizer.housekeeping.voltage.int',
                                        'V-int',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.digitizer.housekeeping.voltage.aux',
                                        'V-aux',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.digitizer.housekeeping.voltage.3V3_synth',
                                        '3v3-synth',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.digitizer.housekeeping.voltage.2V5_synth',
                                        '2V5-synth',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.digitizer.housekeeping.voltage.1V2_adcd',
                                        '1v2-adc',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.digitizer.housekeeping.voltage.3V3',
                                        '3v3',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.digitizer.housekeeping.voltage.3V3_ffly',
                                        '3v3-ffly',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.digitizer.housekeeping.voltage.1V9_adc',
                                        '1v9-adc',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.digitizer.housekeeping.voltage.1V2_adc_a',
                                        '1v2-adc-a',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                },
        },
    'mkat_rsc_states':
        {
            'STATES':
                {
                    'r1':
                        {
                            'M000-M011':
                                [
                                    [
                                        '0',
                                        'rxs.state',
                                        'state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.display-errors',
                                        'errors',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        'rxs.state',
                                        'state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        'rxs.display-errors',
                                        'errors',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '2',
                                        'rxs.state',
                                        'state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '2',
                                        'rxs.display-errors',
                                        'errors',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '3',
                                        'rxs.state',
                                        'state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '3',
                                        'rxs.display-errors',
                                        'errors',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '4',
                                        'rxs.state',
                                        'state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '4',
                                        'rxs.display-errors',
                                        'errors',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '5',
                                        'rxs.state',
                                        'state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '5',
                                        'rxs.display-errors',
                                        'errors',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '6',
                                        'rxs.state',
                                        'state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '6',
                                        'rxs.display-errors',
                                        'errors',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '7',
                                        'rxs.state',
                                        'state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '7',
                                        'rxs.display-errors',
                                        'errors',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'r2':
                        {
                            'M021-M060':
                                [

                                    [
                                        '8',
                                        'rxs.state',
                                        'state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '8',
                                        'rxs.display-errors',
                                        'errors',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '9',
                                        'rxs.state',
                                        'state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '9',
                                        'rxs.display-errors',
                                        'errors',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '10',
                                        'rxs.state',
                                        'state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '10',
                                        'rxs.display-errors',
                                        'errors',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '11',
                                        'rxs.state',
                                        'state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '11',
                                        'rxs.display-errors',
                                        'errors',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '12',
                                        'rxs.state',
                                        'state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '13',
                                        'rxs.display-errors',
                                        'errors',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '14',
                                        'rxs.state',
                                        'state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '14',
                                        'rxs.display-errors',
                                        'errors',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '15',
                                        'rxs.state',
                                        'state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '15',
                                        'rxs.display-errors',
                                        'errors',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '16',
                                        'rxs.state',
                                        'state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '16',
                                        'rxs.display-errors',
                                        'errors',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '17',
                                        'rxs.state',
                                        'state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '17',
                                        'rxs.display-errors',
                                        'errors',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                },
        },
    'mkat_pck_states':
        {
            'STATES':
                {
                    'r1':
                        {
                            'M000-M011':
                                [
                                    [
                                        '0',
                                        'rxs.packetizer.1pps.length',
                                        'pps-length',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.packetizer.1pps.last',
                                        'pps-last',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'rxs.packetizer.freq.refclk1',
                                        'refclk1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        'rxs.packetizer.1pps.length',
                                        'pps-length',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        'rxs.packetizer.1pps.last',
                                        'pps-last',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        'rxs.packetizer.freq.refclk1',
                                        'refclk1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '2',
                                        'rxs.packetizer.1pps.length',
                                        'pps-length',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '2',
                                        'rxs.packetizer.1pps.last',
                                        'pps-last',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '2',
                                        'rxs.packetizer.freq.refclk1',
                                        'refclk1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '3',
                                        'rxs.packetizer.1pps.length',
                                        'pps-length',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '3',
                                        'rxs.packetizer.1pps.last',
                                        'pps-last',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '3',
                                        'rxs.packetizer.freq.refclk1',
                                        'refclk1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '4',
                                        'rxs.packetizer.1pps.length',
                                        'pps-length',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '4',
                                        'rxs.packetizer.1pps.last',
                                        'pps-last',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '4',
                                        'rxs.packetizer.freq.refclk1',
                                        'refclk1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '5',
                                        'rxs.packetizer.1pps.length',
                                        'pps-length',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '5',
                                        'rxs.packetizer.1pps.last',
                                        'pps-last',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '5',
                                        'rxs.packetizer.freq.refclk1',
                                        'refclk1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '6',
                                        'rxs.packetizer.1pps.length',
                                        'pps-length',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '6',
                                        'rxs.packetizer.1pps.last',
                                        'pps-last',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '6',
                                        'rxs.packetizer.freq.refclk1',
                                        'refclk1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '7',
                                        'rxs.packetizer.1pps.length',
                                        'pps-length',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '7',
                                        'rxs.packetizer.1pps.last',
                                        'pps-last',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '7',
                                        'rxs.packetizer.freq.refclk1',
                                        'refclk1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ]
                        },
                    'r2':
                        {
                            'M021-M060':
                                [

                                    [
                                        '8',
                                        'rxs.packetizer.1pps.length',
                                        'pps-length',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '8',
                                        'rxs.packetizer.1pps.last',
                                        'pps-last',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '8',
                                        'rxs.packetizer.freq.refclk1',
                                        'refclk1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '9',
                                        'rxs.packetizer.1pps.length',
                                        'pps-length',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '9',
                                        'rxs.packetizer.1pps.last',
                                        'pps-last',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '9',
                                        'rxs.packetizer.freq.refclk1',
                                        'refclk1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '10',
                                        'rxs.packetizer.1pps.length',
                                        'pps-length',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '10',
                                        'rxs.packetizer.1pps.last',
                                        'pps-last',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '10',
                                        'rxs.packetizer.freq.refclk1',
                                        'refclk1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '11',
                                        'rxs.packetizer.1pps.length',
                                        'pps-length',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '11',
                                        'rxs.packetizer.1pps.last',
                                        'pps-last',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '11',
                                        'rxs.packetizer.freq.refclk1',
                                        'refclk1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '12',
                                        'rxs.packetizer.1pps.length',
                                        'pps-length',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '12',
                                        'rxs.packetizer.1pps.last',
                                        'pps-last',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '12',
                                        'rxs.packetizer.freq.refclk1',
                                        'refclk1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '13',
                                        'rxs.packetizer.1pps.length',
                                        'pps-length',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '13',
                                        'rxs.packetizer.1pps.last',
                                        'pps-last',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '13',
                                        'rxs.packetizer.freq.refclk1',
                                        'refclk1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '14',
                                        'rxs.packetizer.1pps.length',
                                        'pps-length',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '14',
                                        'rxs.packetizer.1pps.last',
                                        'pps-last',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '14',
                                        'rxs.packetizer.freq.refclk1',
                                        'refclk1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '15',
                                        'rxs.packetizer.1pps.length',
                                        'pps-length',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '15',
                                        'rxs.packetizer.1pps.last',
                                        'pps-last',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '15',
                                        'rxs.packetizer.freq.refclk1',
                                        'refclk1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '16',
                                        'rxs.packetizer.1pps.length',
                                        'pps-length',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '16',
                                        'rxs.packetizer.1pps.last',
                                        'pps-last',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '16',
                                        'rxs.packetizer.freq.refclk1',
                                        'refclk1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '17',
                                        'rxs.packetizer.1pps.length',
                                        'pps-length',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '17',
                                        'rxs.packetizer.1pps.last',
                                        'pps-last',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '17',
                                        'rxs.packetizer.freq.refclk1',
                                        'refclk1',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                },
        },
    'tfrgen_full':
        {
            'Control+TX1-TX4':
                {
                    'CONTROL':
                        {
                            'TFRGEN':
                                [
                                    [
                                        '0',
                                        's-band-gen.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'CONTROL':
                                [
                                    [
                                        '0',
                                        's-band-gen.control.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.control.error-state',
                                        'error-state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.control.100m-in-frequency-state',
                                        'state-100m',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.control.pps-in-period',
                                        'pps-in',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.control.version',
                                        'version',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'tx1':
                        {
                            'TX1':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser1.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.error-state',
                                        'error-state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'REFERENCE':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser1.100m-in-frequency-state',
                                        '100m-in',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.pps-in-period',
                                        'pps-in',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.pps-carrier-lock',
                                        'synth-lock',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'CHANNELS':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser1.ch1.pps.opt-power',
                                        'ch1.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch1.100m.opt-power',
                                        'ch1.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch2.pps.opt-power',
                                        'ch2.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch2.100m.opt-power',
                                        'ch2.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch3.pps.opt-power',
                                        'ch3.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch3.100m.opt-power',
                                        'ch3.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch4.pps.opt-power',
                                        'ch4.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch4.100m.opt-power',
                                        'ch4.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch5.pps.opt-power',
                                        'ch5.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch5.100m.opt-power',
                                        'ch5.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch6.pps.opt-power',
                                        'ch6.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch6.100m.opt-power',
                                        'ch6.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch7.pps.opt-power',
                                        'ch7.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch7.100m.opt-power',
                                        'ch7.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch8.pps.opt-power',
                                        'ch8.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch8.100m.opt-power',
                                        'ch8.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'ATC':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser1.atc-device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.atc-activate',
                                        'active',
                                        's-band-gen-laser1-atc-activate',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.atc-temp-stable',
                                        'stable',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.atc-target-temp',
                                        'T-set',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.atc-target-temp-measure',
                                        'T-target',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.atc-sink-temp',
                                        'T-sink',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.atc-supply-voltage',
                                        'V-sup',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.atc-output-voltage',
                                        'V-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.atc-output-current',
                                        'I-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'tx2':
                        {
                            'TX2':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser2.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser2.error-state',
                                        'error-state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'REFERENCE':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser2.100m-in-frequency-state',
                                        '100m-in',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser2.pps-in-period',
                                        'pps-in',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser2.pps-carrier-lock',
                                        'synth-lock',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'CHANNELS':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser2.ch1.pps.opt-power',
                                        'ch1.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser2.ch1.100m.opt-power',
                                        'ch1.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser2.ch2.pps.opt-power',
                                        'ch2.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser2.ch2.100m.opt-power',
                                        'ch2.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser2.ch3.pps.opt-power',
                                        'ch3.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser2.ch3.100m.opt-power',
                                        'ch3.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser2.ch4.pps.opt-power',
                                        'ch4.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser2.ch4.100m.opt-power',
                                        'ch4.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser2.ch5.pps.opt-power',
                                        'ch5.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser2.ch5.100m.opt-power',
                                        'ch5.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser2.ch6.pps.opt-power',
                                        'ch6.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser2.ch6.100m.opt-power',
                                        'ch6.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser2.ch7.pps.opt-power',
                                        'ch7.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser2.ch7.100m.opt-power',
                                        'ch7.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser2.ch8.pps.opt-power',
                                        'ch8.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser2.ch8.100m.opt-power',
                                        'ch8.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'ATC':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser2.atc-device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser2.atc-activate',
                                        'active',
                                        's-band-gen-laser2-atc-activate',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser2.atc-temp-stable',
                                        'stable',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser2.atc-target-temp',
                                        'T-set',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser2.atc-target-temp-measure',
                                        'T-target',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser2.atc-sink-temp',
                                        'T-sink',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser2.atc-supply-voltage',
                                        'V-sup',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser2.atc-output-voltage',
                                        'V-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser2.atc-output-current',
                                        'I-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'tx3':
                        {
                            'TX3':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser3.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser3.error-state',
                                        'error-state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'REFERENCE':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser3.100m-in-frequency-state',
                                        '100m-in',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser3.pps-in-period',
                                        'pps-in',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser3.pps-carrier-lock',
                                        'synth-lock',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'CHANNELS':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser3.ch1.pps.opt-power',
                                        'ch1.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser3.ch1.100m.opt-power',
                                        'ch1.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser3.ch2.pps.opt-power',
                                        'ch2.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser3.ch2.100m.opt-power',
                                        'ch2.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser3.ch3.pps.opt-power',
                                        'ch3.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser3.ch3.100m.opt-power',
                                        'ch3.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser3.ch4.pps.opt-power',
                                        'ch4.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser3.ch4.100m.opt-power',
                                        'ch4.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser3.ch5.pps.opt-power',
                                        'ch5.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser3.ch5.100m.opt-power',
                                        'ch5.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser3.ch6.pps.opt-power',
                                        'ch6.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser3.ch6.100m.opt-power',
                                        'ch6.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser3.ch7.pps.opt-power',
                                        'ch7.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser3.ch7.100m.opt-power',
                                        'ch7.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser3.ch8.pps.opt-power',
                                        'ch8.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser3.ch8.100m.opt-power',
                                        'ch8.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'ATC':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser3.atc-device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser3.atc-activate',
                                        'active',
                                        's-band-gen-laser3-atc-activate',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser3.atc-temp-stable',
                                        'stable',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser3.atc-target-temp',
                                        'T-set',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser3.atc-target-temp-measure',
                                        'T-target',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser3.atc-sink-temp',
                                        'T-sink',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser3.atc-supply-voltage',
                                        'V-sup',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser3.atc-output-voltage',
                                        'V-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser3.atc-output-current',
                                        'I-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'tx4':
                        {
                            'TX4':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser4.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser4.error-state',
                                        'error-state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'REFERENCE':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser4.100m-in-frequency-state',
                                        '100m-in',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser4.pps-in-period',
                                        'pps-in',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser4.pps-carrier-lock',
                                        'synth-lock',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'CHANNELS':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser4.ch1.pps.opt-power',
                                        'ch1.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser4.ch1.100m.opt-power',
                                        'ch1.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser4.ch2.pps.opt-power',
                                        'ch2.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser4.ch2.100m.opt-power',
                                        'ch2.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser4.ch3.pps.opt-power',
                                        'ch3.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser4.ch3.100m.opt-power',
                                        'ch3.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser4.ch4.pps.opt-power',
                                        'ch4.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser4.ch4.100m.opt-power',
                                        'ch4.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser4.ch5.pps.opt-power',
                                        'ch5.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser4.ch5.100m.opt-power',
                                        'ch5.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser4.ch6.pps.opt-power',
                                        'ch6.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser4.ch6.100m.opt-power',
                                        'ch6.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser4.ch7.pps.opt-power',
                                        'ch7.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser4.ch7.100m.opt-power',
                                        'ch7.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser4.ch8.pps.opt-power',
                                        'ch8.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser4.ch8.100m.opt-power',
                                        'ch8.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'ATC':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser4.atc-device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser4.atc-activate',
                                        'active',
                                        's-band-gen-laser4-atc-activate',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser4.atc-temp-stable',
                                        'stable',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser4.atc-target-temp',
                                        'T-set',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser4.atc-target-temp-measure',
                                        'T-target',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser4.atc-sink-temp',
                                        'T-sink',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser4.atc-supply-voltage',
                                        'V-sup',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser4.atc-output-voltage',
                                        'V-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser4.atc-output-current',
                                        'I-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                },
            'TX5-TX9':
                {
                    'tx5':
                        {
                            'tx5':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser5.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.error-state',
                                        'error-state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'REFERENCE':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser5.100m-in-frequency-state',
                                        '100m-in',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.pps-in-period',
                                        'pps-in',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.pps-carrier-lock',
                                        'synth-lock',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'CHANNELS':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser5.ch1.pps.opt-power',
                                        'ch1.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch1.100m.opt-power',
                                        'ch1.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch2.pps.opt-power',
                                        'ch2.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch2.100m.opt-power',
                                        'ch2.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch3.pps.opt-power',
                                        'ch3.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch3.100m.opt-power',
                                        'ch3.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch4.pps.opt-power',
                                        'ch4.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch4.100m.opt-power',
                                        'ch4.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch5.pps.opt-power',
                                        'ch5.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch5.100m.opt-power',
                                        'ch5.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch6.pps.opt-power',
                                        'ch6.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch6.100m.opt-power',
                                        'ch6.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch7.pps.opt-power',
                                        'ch7.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch7.100m.opt-power',
                                        'ch7.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch8.pps.opt-power',
                                        'ch8.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch8.100m.opt-power',
                                        'ch8.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'ATC':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser5.atc-device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.atc-activate',
                                        'active',
                                        's-band-gen-laser5-atc-activate',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.atc-temp-stable',
                                        'stable',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.atc-target-temp',
                                        'T-set',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.atc-target-temp-measure',
                                        'T-target',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.atc-sink-temp',
                                        'T-sink',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.atc-supply-voltage',
                                        'V-sup',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.atc-output-voltage',
                                        'V-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.atc-output-current',
                                        'I-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'tx6':
                        {
                            'tx6':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser6.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser6.error-state',
                                        'error-state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'REFERENCE':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser6.100m-in-frequency-state',
                                        '100m-in',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser6.pps-in-period',
                                        'pps-in',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser6.pps-carrier-lock',
                                        'synth-lock',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'CHANNELS':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser6.ch1.pps.opt-power',
                                        'ch1.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser6.ch1.100m.opt-power',
                                        'ch1.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser6.ch2.pps.opt-power',
                                        'ch2.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser6.ch2.100m.opt-power',
                                        'ch2.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser6.ch3.pps.opt-power',
                                        'ch3.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser6.ch3.100m.opt-power',
                                        'ch3.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser6.ch4.pps.opt-power',
                                        'ch4.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser6.ch4.100m.opt-power',
                                        'ch4.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser6.ch5.pps.opt-power',
                                        'ch5.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser6.ch5.100m.opt-power',
                                        'ch5.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser6.ch6.pps.opt-power',
                                        'ch6.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser6.ch6.100m.opt-power',
                                        'ch6.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser6.ch7.pps.opt-power',
                                        'ch7.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser6.ch7.100m.opt-power',
                                        'ch7.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser6.ch8.pps.opt-power',
                                        'ch8.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser6.ch8.100m.opt-power',
                                        'ch8.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'ATC':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser6.atc-device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser6.atc-activate',
                                        'active',
                                        's-band-gen-laser6-atc-activate',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser6.atc-temp-stable',
                                        'stable',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser6.atc-target-temp',
                                        'T-set',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser6.atc-target-temp-measure',
                                        'T-target',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser6.atc-sink-temp',
                                        'T-sink',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser6.atc-supply-voltage',
                                        'V-sup',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser6.atc-output-voltage',
                                        'V-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser6.atc-output-current',
                                        'I-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'tx7':
                        {
                            'tx7':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser7.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser7.error-state',
                                        'error-state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'REFERENCE':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser7.100m-in-frequency-state',
                                        '100m-in',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser7.pps-in-period',
                                        'pps-in',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser7.pps-carrier-lock',
                                        'synth-lock',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'CHANNELS':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser7.ch1.pps.opt-power',
                                        'ch1.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser7.ch1.100m.opt-power',
                                        'ch1.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser7.ch2.pps.opt-power',
                                        'ch2.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser7.ch2.100m.opt-power',
                                        'ch2.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser7.ch3.pps.opt-power',
                                        'ch3.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser7.ch3.100m.opt-power',
                                        'ch3.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser7.ch4.pps.opt-power',
                                        'ch4.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser7.ch4.100m.opt-power',
                                        'ch4.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser7.ch5.pps.opt-power',
                                        'ch5.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser7.ch5.100m.opt-power',
                                        'ch5.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser7.ch6.pps.opt-power',
                                        'ch6.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser7.ch6.100m.opt-power',
                                        'ch6.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser7.ch7.pps.opt-power',
                                        'ch7.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser7.ch7.100m.opt-power',
                                        'ch7.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser7.ch8.pps.opt-power',
                                        'ch8.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser7.ch8.100m.opt-power',
                                        'ch8.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'ATC':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser7.atc-device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser7.atc-activate',
                                        'active',
                                        's-band-gen-laser7-atc-activate',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser7.atc-temp-stable',
                                        'stable',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser7.atc-target-temp',
                                        'T-set',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser7.atc-target-temp-measure',
                                        'T-target',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser7.atc-sink-temp',
                                        'T-sink',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser7.atc-supply-voltage',
                                        'V-sup',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser7.atc-output-voltage',
                                        'V-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser7.atc-output-current',
                                        'I-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'tx8':
                        {
                            'tx8':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser8.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser8.error-state',
                                        'error-state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'REFERENCE':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser8.100m-in-frequency-state',
                                        '100m-in',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser8.pps-in-period',
                                        'pps-in',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser8.pps-carrier-lock',
                                        'synth-lock',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'CHANNELS':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser8.ch1.pps.opt-power',
                                        'ch1.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser8.ch1.100m.opt-power',
                                        'ch1.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser8.ch2.pps.opt-power',
                                        'ch2.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser8.ch2.100m.opt-power',
                                        'ch2.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser8.ch3.pps.opt-power',
                                        'ch3.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser8.ch3.100m.opt-power',
                                        'ch3.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser8.ch4.pps.opt-power',
                                        'ch4.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser8.ch4.100m.opt-power',
                                        'ch4.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser8.ch5.pps.opt-power',
                                        'ch5.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser8.ch5.100m.opt-power',
                                        'ch5.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser8.ch6.pps.opt-power',
                                        'ch6.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser8.ch6.100m.opt-power',
                                        'ch6.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser8.ch7.pps.opt-power',
                                        'ch7.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser8.ch7.100m.opt-power',
                                        'ch7.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser8.ch8.pps.opt-power',
                                        'ch8.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser8.ch8.100m.opt-power',
                                        'ch8.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'ATC':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser8.atc-device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser8.atc-activate',
                                        'active',
                                        's-band-gen-laser8-atc-activate',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser8.atc-temp-stable',
                                        'stable',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser8.atc-target-temp',
                                        'T-set',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser8.atc-target-temp-measure',
                                        'T-target',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser8.atc-sink-temp',
                                        'T-sink',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser8.atc-supply-voltage',
                                        'V-sup',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser8.atc-output-voltage',
                                        'V-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser8.atc-output-current',
                                        'I-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'tx9':
                        {
                            'tx9':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser9.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser9.error-state',
                                        'error-state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'REFERENCE':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser9.100m-in-frequency-state',
                                        '100m-in',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser9.pps-in-period',
                                        'pps-in',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser9.pps-carrier-lock',
                                        'synth-lock',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'CHANNELS':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser9.ch1.pps.opt-power',
                                        'ch1.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser9.ch1.100m.opt-power',
                                        'ch1.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser9.ch2.pps.opt-power',
                                        'ch2.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser9.ch2.100m.opt-power',
                                        'ch2.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser9.ch3.pps.opt-power',
                                        'ch3.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser9.ch3.100m.opt-power',
                                        'ch3.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser9.ch4.pps.opt-power',
                                        'ch4.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser9.ch4.100m.opt-power',
                                        'ch4.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser9.ch5.pps.opt-power',
                                        'ch5.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser9.ch5.100m.opt-power',
                                        'ch5.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser9.ch6.pps.opt-power',
                                        'ch6.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser9.ch6.100m.opt-power',
                                        'ch6.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser9.ch7.pps.opt-power',
                                        'ch7.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser9.ch7.100m.opt-power',
                                        'ch7.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser9.ch8.pps.opt-power',
                                        'ch8.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser9.ch8.100m.opt-power',
                                        'ch8.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'ATC':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser9.atc-device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser9.atc-activate',
                                        'active',
                                        's-band-gen-laser9-atc-activate',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser9.atc-temp-stable',
                                        'stable',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser9.atc-target-temp',
                                        'T-set',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser9.atc-target-temp-measure',
                                        'T-target',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser9.atc-sink-temp',
                                        'T-sink',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser9.atc-supply-voltage',
                                        'V-sup',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser9.atc-output-voltage',
                                        'V-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser9.atc-output-current',
                                        'I-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                },
        },
    'tfrgen_eff':
        {
            'Control+TX1+TX4':
                {
                    'tfrgen_control':
                        {
                            'tfrgen':
                                [
                                    [
                                        '0',
                                        's-band-gen.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'CONTROL':
                                [
                                    [
                                        '0',
                                        's-band-gen.control.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.control.error-state',
                                        'error-state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.control.100m-in-frequency-state',
                                        'state-100m',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.control.pps-in-period',
                                        'pps-in',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.control.version',
                                        'version',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'tx1':
                        {
                            'TX1':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser1.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.error-state',
                                        'error-state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'REFERENCE':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser1.100m-in-frequency-state',
                                        '100m-in',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.pps-in-period',
                                        'pps-in',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.pps-carrier-lock',
                                        'synth-lock',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'CHANNELS':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser1.ch1.pps.opt-power',
                                        'ch1.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch1.100m.opt-power',
                                        'ch1.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch2.pps.opt-power',
                                        'ch2.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch2.100m.opt-power',
                                        'ch2.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch3.pps.opt-power',
                                        'ch3.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch3.100m.opt-power',
                                        'ch3.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch4.pps.opt-power',
                                        'ch4.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch4.100m.opt-power',
                                        'ch4.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch5.pps.opt-power',
                                        'ch5.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch5.100m.opt-power',
                                        'ch5.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch6.pps.opt-power',
                                        'ch6.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch6.100m.opt-power',
                                        'ch6.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch7.pps.opt-power',
                                        'ch7.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch7.100m.opt-power',
                                        'ch7.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch8.pps.opt-power',
                                        'ch8.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.ch8.100m.opt-power',
                                        'ch8.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'ATC':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser1.atc-device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.atc-activate',
                                        'active',
                                        's-band-gen-laser4-atc-activate',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.atc-temp-stable',
                                        'stable',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.atc-target-temp',
                                        'T-set',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.atc-target-temp-measure',
                                        'T-target',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.atc-sink-temp',
                                        'T-sink',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.atc-supply-voltage',
                                        'V-sup',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.atc-output-voltage',
                                        'V-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser1.atc-output-current',
                                        'I-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                    'tx5':
                        {
                            'TX5':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser5.device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.error-state',
                                        'error-state',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'REFERENCE':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser5.100m-in-frequency-state',
                                        '100m-in',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.pps-in-period',
                                        'pps-in',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.pps-carrier-lock',
                                        'synth-lock',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'CHANNELS':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser5.ch1.pps.opt-power',
                                        'ch1.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch1.100m.opt-power',
                                        'ch1.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch2.pps.opt-power',
                                        'ch2.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch2.100m.opt-power',
                                        'ch2.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch3.pps.opt-power',
                                        'ch3.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch3.100m.opt-power',
                                        'ch3.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch4.pps.opt-power',
                                        'ch4.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch4.100m.opt-power',
                                        'ch4.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch5.pps.opt-power',
                                        'ch5.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch5.100m.opt-power',
                                        'ch5.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch6.pps.opt-power',
                                        'ch6.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch6.100m.opt-power',
                                        'ch6.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch7.pps.opt-power',
                                        'ch7.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch7.100m.opt-power',
                                        'ch7.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch8.pps.opt-power',
                                        'ch8.pps-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.ch8.100m.opt-power',
                                        'ch8.100m-opt',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'ATC':
                                [
                                    [
                                        '0',
                                        's-band-gen.laser5.atc-device-status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.atc-activate',
                                        'active',
                                        's-band-gen-laser4-atc-activate',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.atc-temp-stable',
                                        'stable',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.atc-target-temp',
                                        'T-set',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.atc-target-temp-measure',
                                        'T-target',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.atc-sink-temp',
                                        'T-sink',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.atc-supply-voltage',
                                        'V-sup',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.atc-output-voltage',
                                        'V-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        's-band-gen.laser5.atc-output-current',
                                        'I-out',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                },
        },
    'ept@eff':
        {
            'TICs':  # Clipboard_0
                {
                    'tics':
                        {
                            'tic_round_trip_focus':
                                [
                                    [
                                        '0',
                                        'tic_round_trip_focus.control.device_status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'tic_round_trip_focus.control.error_status',
                                        'error-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'tic_round_trip_focus.control.connection_status',
                                        'connected',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'tic_round_trip_focus.read',
                                        'read',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'tic_lant1_i3000':
                                [
                                    [
                                        '1',
                                        'tic_lant1_i3000.control.device_status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        'tic_lant1_i3000.control.error_status',
                                        'error-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        'tic_lant1_i3000.control.connection_status',
                                        'connected',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '1',
                                        'tic_lant1_i3000.read',
                                        'read',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'tic_lant1_lant2':
                                [
                                    [
                                        '2',
                                        'tic_lant1_lant2.control.device_status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '2',
                                        'tic_lant1_lant2.control.error_status',
                                        'error-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '2',
                                        'tic_lant1_lant2.control.connection_status',
                                        'connected',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '2',
                                        'tic_lant1_lant2.read',
                                        'read',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'tic_i3000_i45_pps':
                                [
                                    [
                                        '3',
                                        'tic_i3000_i45_pps.control.device_status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '3',
                                        'tic_i3000_i45_pps.control.error_status',
                                        'error-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '3',
                                        'tic_i3000_i45_pps.control.connection_status',
                                        'connected',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '3',
                                        'tic_i3000_i45_pps.read',
                                        'read',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'tic_lant1_i45':
                                [
                                    [
                                        '4',
                                        'tic_lant1_i45.control.device_status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '4',
                                        'tic_lant1_i45.control.error_status',
                                        'error-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '4',
                                        'tic_lant1_i45.control.connection_status',
                                        'connected',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '4',
                                        'tic_lant1_i45.read',
                                        'read',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                            'tic_i3000_i45_100m':
                                [
                                    [
                                        '5',
                                        'tic_i3000_i45_100m.control.device_status',
                                        'device-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '5',
                                        'tic_i3000_i45_100m.control.error_status',
                                        'error-status',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '5',
                                        'tic_i3000_i45_100m.control.connection_status',
                                        'connected',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '5',
                                        'tic_i3000_i45_100m.read',
                                        'read',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                },
        },
    'intercom':
        {
            'ICOM':  # Clipboard_0
                {
                    'col1':  # col 1
                        {
                            'TEST':
                                [
                                    [
                                        '0',
                                        'multicbe.backend.ch01_sb_tpa',
                                        'ch01_sb_tpa',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                    [
                                        '0',
                                        'multicbe.backend.ch02_sb_tpb',
                                        'ch02_sb_tpb',
                                        '',
                                        '',
                                        'cell'
                                    ],
                                ],
                        },
                },
        },
}
